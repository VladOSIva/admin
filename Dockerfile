FROM node:latest as build-deps
WORKDIR /usr/src/app
ENV NODE_OPTIONS --max-old-space-size=4096
COPY package.json ./
RUN npm install
COPY . ./
RUN npm run-script --max_old_space_size=4096 build

FROM nginx:1.18-alpine
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
COPY --from=build-deps /usr/src/app/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
