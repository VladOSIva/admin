import React from 'react';

import { Card, CardBody, Container, Alert} from "reactstrap";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';

// Redux 
import {connect} from 'react-redux';

// availity-reactstrap-validation
import { AvForm, AvField } from 'availity-reactstrap-validation';
// actions
import { createUser, apiError } from '../../store/userManage/createUser/actions';
const CreatNewUserComponent = ({createUser, apiError, CreateNewUser, history}) => {


    function  handleValidSubmit(event, values) {
            createUser(values, history);
            console.log(values)
    }

    return (

        
          <React.Fragment>
          <div className="page-content">
          {CreateNewUser.success == false ? (
              <>
              {Object.values(CreateNewUser.error).map(v => 
                <Alert color="danger">{v}</Alert> 
                )}
              </>

          ): null}
                    <Container fluid={true}>

                        <Breadcrumbs title="Создание пользователя" breadcrumbItem="Администрирование" />
                        <Card>
                            <CardBody>
                            <AvForm className="form-horizontal" onValidSubmit={(e,v) => { handleValidSubmit(e,v) }}>


                            <div className="form-group">
                                <AvField name="name" label="Имя" className="form-control" placeholder="Введите Имя/Фамилия" type="text" required />
                            </div>

                            <div className="form-group">
                            <AvField name="email" label="Email" className="form-control" placeholder="Email" type="email" required />
                            </div>

                            <div className="form-group">
                                <AvField name="password" label="Пароль" type="password" required placeholder="Пароль" />
                            </div> 
                            <div className="form-group">
                                <AvField name="confirm_password" label="Повторите пароль" type="password" required placeholder="Пароль" />
                            </div>


                            <div className="mt-3">
                                <button className="btn btn-primary btn-block waves-effect waves-light" type="submit">Создать</button>
                            </div>

                        </AvForm>
                        </CardBody>
                      </Card>
                    </Container>
                </div>
            </React.Fragment> 
          );
    }

const mapStateToProps = state => {
    return {
        CreateNewUser: state.CreateNewUser
    }
}
export default connect(mapStateToProps,{createUser, apiError})(CreatNewUserComponent);
