import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, CardBody, FormGroup, Label,CardTitle, Button, Alert } from "reactstrap";
import { AvForm, AvField, AvInput } from "availity-reactstrap-validation";
import "@vtaits/react-color-picker/dist/index.css";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';
// Redux 
import { connect } from 'react-redux';
import {fetchUser, updateUser, fetchUserError,successUpdateUserError,deleteUser} from '../../store/userManage/updateUser/actions';

const ManageUser = ({fetchUser, UpdateUser,updateUser,history, deleteUser}) => {
	//Set id to Redux
	const urlId = window.location.pathname.split('/administration/user_update/')[1];

	const [optionsSelect, setoptionsSelect] = useState([{
		label: "Права",
		options:[
			{ label: "Manager", value: 1 },
			{ label: "Admin", value: 0 },
		]
	}]);

	
	useEffect(() => {
		fetchUser(urlId)
	},[]);

  
  const [formData, setFormData] = useState({
	  name:'',
	  email:'',
	  password:'',
	  confirm_password:'',
	  permission:[]
  });

  useEffect(() => {
	  if (UpdateUser.fetchSuccess === true) {
		 setFormData({...formData, name:UpdateUser.fetchUser.data.name, email: UpdateUser.fetchUser.data.email})
		}
	},[UpdateUser.fetchUser.data]);

  const {name, email, password, confirm_password} = formData;

  const onChange = e => setFormData({...formData, [e.target.name]: e.target.value});

	const Update = (e) => {
		e.preventDefault()
		updateUser(urlId, formData, history)
	};

	const Delete = (e) => {
		e.preventDefault()
		deleteUser(urlId, history)
	};

	function handleSelectGroup(selectedGroup) { setFormData({...formData, permission: selectedGroup.value}); };
    return (
    	 <React.Fragment>
				<div className="page-content">
					<Container fluid={true}>
						<Breadcrumbs title="Управление пользователями" breadcrumbItem="Администрирование" />
						<Row>
						<Col lg={12}>
						{UpdateUser.updateSuccess == false ? (
							<>
							{Object.values(UpdateUser.error.errors).map((v,k) => 
							  <Alert key={k} color="danger">{v}</Alert> 
							  )}
							</>
							): null}
						<Card>
						  
							<CardBody>
							  <CardTitle>Обновить/Удалить пользователя</CardTitle>

							  <AvForm>
								<AvField
								  name="name"
								  value={name}
								  label="Имя/Фамилия  "
								  placeholder="Type Something"
								  type="text"
								  onChange={e => onChange(e)}
								  errorMessage="Введите имя"
								  validate={{ required: { value: true } }}
								/>
								<AvField
									name="email"
									value={email}
									label="E-Mail  "
									placeholder="Enter Valid Email"
									type="email"
									onChange={e => onChange(e)}
									errorMessage="Invalid Email"
									validate={{
									required: { value: true },
									email: { value: true }
									}}
							  />
							  <Label>Пароль</Label>

								<AvField
								  name="password"
								  type="password"
								  value={password}
								  onChange={e => onChange(e)}
								  placeholder="Password"
								  errorMessage="Введите пароль"
								  validate={{ required: { value: true } }}
								/>
								<Label>Повторите пароль</Label>
								<AvField
								  name="confirm_password"
								  type="password"
								  value={confirm_password}
								  onChange={e => onChange(e)}
								  placeholder="Повторите пароль"
								  errorMessage="Повторите пароль"
								  validate={{
									required: { value: true },
									match: { value: "password" }
								  }}
								/>
							
								<FormGroup className="select2-container">
								<Label>Права пользователя</Label>
								<Select
									name="premisions"
									onChange={e => handleSelectGroup(e)}
									options={optionsSelect}
									classNamePrefix="select2-selection"
								/>

								</FormGroup>
							
							  </AvForm>
							</CardBody>
						  </Card>
						</Col>
					  </Row>
		  
						<Button 
							 color="primary"
                        	 outline 
							className="btn btn-lg btn-block waves-effect waves-light "    
							onClick={Update}
			   			>Редактировть</Button>
						<Button  
						   	color="danger"
                        	outline 
							className="btn btn-lg btn-block waves-effect waves-light "    
							onClick={Delete}
               			>Удалить</Button>
					</Container>
				</div>
			</React.Fragment>
		  );
	}
	
const mapStateToProps = state => {
	return {
		UpdateUser: state.UpdateUser,
	}
}

export default connect(mapStateToProps,{fetchUser, updateUser, fetchUserError,successUpdateUserError,deleteUser})(ManageUser);
