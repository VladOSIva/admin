import React, {Fragment, useEffect, useState} from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col, Card, CardBody, Alert, Button } from "reactstrap";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';

// Redux 
import { connect } from 'react-redux';
//Actoins 
import { fetchUsers, errorFetch} from '../../store/userManage/listUsers/actions';
import { MDBDataTable } from "mdbreact";
// notification


const UserList = ({fetchUsers, FetchUsersList:{users, loading}, UpdateUser:{updateSuccess, deleteSuccess, fetchUser}, CreateNewUser:{user, success}}) => {

        useEffect(() => {
            fetchUsers()
        },[fetchUsers])
        // Data fetching 
        const data = {
            columns: [
                  {
                    label: "ID",
                    field: "id",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Имя",
                    field: "name",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Email",
                    field: "email",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Действие",
                    field: "action",
                    sort: "asc",
                    width: 150
                  }
            ],
            rows: [
                {
                }
            ]
        }
        if (loading === false ) {
            users.data.map(v => {
                data.rows.push({
                    id: v.id,
                    name: v.name,
                    email: v.email,
                    action: <Link to={`/administration/user_update/${v.id}`}>Изменить</Link>
                })
            })
        }
    return (
             <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumbs */}
                        <Breadcrumbs title="Contacts" breadcrumbItem="Users List" />
                        {
                            loading !== false ? (
                                <>...loading</>
                            ) : (

                            <Row>
                            <Col lg="12">
                            <Link to='/administration/user'>
                                <Button  color="primary"
                                outline 
                                className="btn mb-2 btn-lg btn-block waves-effect waves-light "    
                                >Создание пользователя</Button>
                                </Link>
                                <Card>
                                {(updateSuccess === true) ? (
                                    <div className="position-absolute mr-3"  
                                      style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="success" className="mb-0" role="alert">
                                            Пользователь:{fetchUser.data.name}, обновлен.
                                        </Alert>
                                    </div>
                                ) : null}
                                {(success === true) ? (
                                    <div className="position-absolute mr-3"  
                                      style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="success" className="mb-0" role="alert">
                                            Пользователь:{user.user.name}, создан.
                                        </Alert>
                                        </div>
                                ) : null}
                                {(deleteSuccess === true) ? (
                                    <div className="position-absolute mr-3"  
                                    style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="danger" className="mb-0" role="alert">
                                            Пользователь:{fetchUser.data.name}, был удален.
                                        </Alert>
                                    </div>
                                ) : null}
                                    <CardBody>
                                         <MDBDataTable responsive bordered data={data} />
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                        )
                    }
                    </Container>
                </div>
            </React.Fragment>
          );
    }

const mapStateToProps = state => {
    return {
        FetchUsersList: state.FetchUsersList,
        UpdateUser: state.UpdateUser,
        CreateNewUser: state.CreateNewUser
    }
}
export default connect(mapStateToProps, {fetchUsers, errorFetch})(UserList);