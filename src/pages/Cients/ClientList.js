import React, {Fragment, useEffect, useState} from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col, Card, CardBody, Alert, Button } from "reactstrap";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';

// Redux 
import { connect } from 'react-redux';
//Actoins 
import { fetchClients, errorFetch } from '../../store/clientsManage/listClients/actions';
import { MDBDataTable } from "mdbreact";
// notification


const UserList = ({fetchClients, FetchClientsList:{clients, loading}, UpdateClient:{updateSuccess, deleteSuccess, fetchClient}, CreateNewClient:{client, success}}) => {

        useEffect(() => {
            fetchClients()
        },[fetchClients])
        // Data fetching 
        const data = {
            columns: [
                  {
                    label: "ID",
                    field: "id",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Имя",
                    field: "name",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Email",
                    field: "email",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Логин",
                    field: "login",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Телефон",
                    field: "phone",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Referral_id",
                    field: "referral_id",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Действие",
                    field: "action",
                    sort: "asc",
                    width: 150
                  }
            ],
            rows: [
                {
                }
            ]
        }
        if (loading === false ) {
            clients.data.map(v => {
                data.rows.push({
                    id: v.id,
                    name: `${v.first_name} ${v.last_name}`,
                    email: v.email,
                    login: v.login,
                    phone: v.phone,
                    referral_id: v.referral_id,
                    action: <Link to={`/administration/client_update/${v.id}`}>Изменить</Link>
                })
            })
        }
    return (
             <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumbs */}
                        <Breadcrumbs title="Contacts" breadcrumbItem="Users List" />
                        {
                            loading !== false ? (
                                <>...loading</>
                            ) : (

                            <Row>
                            <Col lg="12">
                            <Link to='/administration/client'>
                                <Button  color="primary"
                                outline 
                                className="btn mb-2 btn-lg btn-block waves-effect waves-light "    
                                >Создание клиента</Button>
                                </Link>
                                <Card>
                                {(updateSuccess === true) ? (
                                    <div className="position-absolute mr-3"  
                                      style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="success" className="mb-0" role="alert">
                                            Пользователь:{fetchClient.data.first_name} {fetchClient.data.last_name}, обновлен.
                                        </Alert>
                                    </div>
                                ) : null}
                                {(success === true) ? (
                                    <div className="position-absolute mr-3"  
                                      style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="success" className="mb-0" role="alert">
                                            Клиент:{client.client.first_name} {client.client.last_name}, создан.
                                        </Alert>
                                        </div>
                                ) : null}
                                {(deleteSuccess === true) ? (
                                    <div className="position-absolute mr-3"  
                                    style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="danger" className="mb-0" role="alert">
                                            Пользователь:{fetchClient.data.first_name} {fetchClient.data.last_name}, был удален.
                                        </Alert>
                                    </div>
                                ) : null}
                                    <CardBody>
                                         <MDBDataTable responsive bordered data={data} />
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                        )
                    }
                    </Container>
                </div>
            </React.Fragment>
          );
    }

const mapStateToProps = state => {
    return {
        FetchClientsList: state.FetchClientsList,
        UpdateClient: state.UpdateClient,
        CreateNewClient: state.CreateNewClient
    }
}
export default connect(mapStateToProps, {fetchClients, errorFetch})(UserList);