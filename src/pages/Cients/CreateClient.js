import React from 'react';

import { Card, CardBody, Container, Alert} from "reactstrap";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';

// Redux 
import {connect} from 'react-redux';

// availity-reactstrap-validation
import { AvForm, AvField } from 'availity-reactstrap-validation';
// actions
import { createClient, apiErrorClient } from '../../store/clientsManage/createClient/actions';
const CreatNewUserComponent = ({createClient, apiErrorClient, CreateNewClient, history}) => {


    function  handleValidSubmit(event, values) {
            createClient(values, history);
    }
    return (

        
          <React.Fragment>
          <div className="page-content">
          {CreateNewClient.success == false ? (
              <>
              {Object.values(CreateNewClient.error).map(v => 
                <Alert color="danger">{v}</Alert> 
                )}
              </>

          ): null}
                    <Container fluid={true}>

                        <Breadcrumbs title="Создание клиента" breadcrumbItem="Администрирование" />
                        <Card>
                            <CardBody>
                            <AvForm className="form-horizontal" onValidSubmit={(e,v) => { handleValidSubmit(e,v) }}>


                            <div className="form-group">
                                <AvField name="first_name" label="Имя" className="form-control" placeholder="Введите имя" type="text" required />
                            </div>

                            <div className="form-group">
                            <AvField name="last_name" label="Фамилия" className="form-control" placeholder="Введите фамилию" type="text" required />
                            </div>


                            <div className="form-group">
                            <AvField name="email" label="Email" className="form-control" placeholder="Email" type="email" required />
                            </div>

                            
                            <div className="form-group">
                            <AvField name="login" label="Логин" className="form-control" placeholder="Введите логин" type="text" required />
                            </div>

                               
                            <div className="form-group">
                            <AvField
                                    name="phone"
                                    label="Телефон"
                                    className="form-control"
                                    placeholder="Введите телефон" type="number" 
                                    errorMessage="Только цифры"
                                    validate={{
                                    required: { value: true },
                                    pattern: {
                                        value: "^[0-9]+$",
                                        errorMessage: "Только цифры"
                                    }
                                    }} 
                                />
                            </div>

                            <div className="form-group">
                                <AvField name="password" label="Пароль" type="password" required placeholder="Пароль" />
                            </div> 
                            <div className="form-group">
                                <AvField name="confirm_password" label="Повторите пароль" type="password" required placeholder="Пароль" />
                            </div>


                            <div className="mt-3">
                                <button className="btn btn-primary btn-block waves-effect waves-light" type="submit">Создать</button>
                            </div>

                        </AvForm>
                        </CardBody>
                      </Card>
                    </Container>
                </div>
            </React.Fragment> 
          );
    }

const mapStateToProps = state => {
    return {
        CreateNewClient: state.CreateNewClient
    }
}
export default connect(mapStateToProps,{createClient, apiErrorClient})(CreatNewUserComponent);
