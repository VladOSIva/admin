import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, CardBody, Form, FormGroup, InputGroup, Label,CardTitle, Input, Button, CardSubtitle, Alert } from "reactstrap";
import { AvForm, AvField, AvInput } from "availity-reactstrap-validation";
import "@vtaits/react-color-picker/dist/index.css";
import "react-datepicker/dist/react-datepicker.css";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';
// Redux 
import { connect } from 'react-redux';

import {fetchClient, updateClient, fetchClientError, successUpdateClientError, deleteClient} from '../../store/clientsManage/updateClient/actions';

const ManageClient = ({fetchClient, UpdateClient, updateClient,history, deleteClient}) => {
	//Set id to Redux
	const clientId = window.location.pathname.split('/administration/client_update/')[1]
	useEffect(() => {
		fetchClient(clientId)
	},[])

  
  const [formData, setFormData] = useState({
	  first_name:'',
      last_name:'',
      email: '',
      login: '',
      phone: '',
	  password:'',
	  confirm_password:''
  })

  useEffect(() => {
	  if (UpdateClient.fetchSuccess === true) {
		 setFormData({
                ...formData,
                first_name:UpdateClient.fetchClient.data.first_name,
                last_name:UpdateClient.fetchClient.data.last_name,
                email: UpdateClient.fetchClient.data.email,
                login: UpdateClient.fetchClient.data.login,
                phone: UpdateClient.fetchClient.data.phone
            })
		}
	},[UpdateClient.fetchClient.data])

  const {first_name, last_name, email, login, phone, password, confirm_password} = formData

  const onChange = e => setFormData({...formData, [e.target.name]: e.target.value})

	const Update = (e) => {
		e.preventDefault()
        updateClient(clientId, formData, history)
	}

	const Delete = (e) => {
		e.preventDefault()
		deleteClient(clientId, history)
	}

    return (
    	 <React.Fragment>
				<div className="page-content">
					<Container fluid={true}>
						<Breadcrumbs title="Управление пользователями" breadcrumbItem="Администрирование" />
						<Row>
						<Col lg={12}>
						{UpdateClient.updateSuccess == false ? (
							<>
							{Object.values(UpdateClient.error.errors).map((v,k) => 
							  <Alert key={k} color="danger">{v}</Alert> 
							  )}
							</>
							): null}
						<Card>
						  
							<CardBody>
							  <CardTitle>Обновить/Удалить клиента</CardTitle>

							  <AvForm>
								<AvField
								  name="first_name"
								  value={first_name}
								  label="Имя"
								  placeholder="Имя"
								  type="text"
								  onChange={e => onChange(e)}
								  errorMessage="Введите имя"
								  validate={{ required: { value: true } }}
                                />
                                <AvField
                                    name="last_name"
                                    value={last_name}
                                    label="Фамилия  "
                                    placeholder="Фамилия"
                                    type="text"
                                    onChange={e => onChange(e)}
                                    errorMessage="Введите имя"
                                    validate={{ required: { value: true } }}
                              />
								<AvField
									name="email"
									value={email}
									label="E-Mail  "
									placeholder="E-Mail"
									type="email"
									onChange={e => onChange(e)}
									errorMessage="Invalid Email"
									validate={{
									required: { value: true },
									email: { value: true }
									}}
                              />
                              <AvField
                                name="login"
                                value={login}
                                label="Логин  "
                                placeholder="Логин"
                                type="text"
                                onChange={e => onChange(e)}
                                errorMessage="Введите логин"
                                validate={{ required: { value: true } }}
                            />
                            <AvField
                                    name="phone"
                                    label="Телефон"
                                    onChange={e => onChange(e)}
                                    value={phone}
                                    className="form-control"
                                    placeholder="Введите телефон" type="number" 
                                    errorMessage="Только цифры"
                                    validate={{
                                    required: { value: true },
                                    pattern: {
                                        value: "^[0-9]+$",
                                        errorMessage: "Только цифры"
                                    }
                                    }} 
                                />
							  <Label>Пароль</Label>

								<AvField
								  name="password"
								  type="password"
								  value={password}
								  onChange={e => onChange(e)}
								  placeholder="Введите пароль"
								  errorMessage="Введите пароль"
								/>
								<Label>Повторите пароль</Label>

								<AvField
								  name="confirm_password"
								  type="password"
								  value={confirm_password}
								  onChange={e => onChange(e)}
								  placeholder="Повторите пароль"
								  errorMessage="Повторите пароль"
								/>
							
							  </AvForm>
							</CardBody>
						  </Card>
						</Col>
					  </Row>
		  
						<Button 
							 color="primary"
                        	 outline 
							className="btn btn-lg btn-block waves-effect waves-light "    
							onClick={Update}
			   			>Редактировть</Button>
						<Button  
						   	color="danger"
                        	outline 
							className="btn btn-lg btn-block waves-effect waves-light "    
							onClick={Delete}
               			>Удалить</Button>
					</Container>
				</div>
			</React.Fragment>
		  );
	}
	
const mapStateToProps = state => {
	return {
		UpdateClient: state.UpdateClient,
	}
}

export default connect(mapStateToProps,{fetchClient, updateClient, fetchClientError,successUpdateClientError,deleteClient})(ManageClient);
