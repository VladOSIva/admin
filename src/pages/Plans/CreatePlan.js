import React, { useState } from 'react';

import { Card, CardBody, Container, Alert,  Label} from "reactstrap";
import Select from "react-select";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';

// Redux 
import {connect} from 'react-redux';

// availity-reactstrap-validation
import { AvForm, AvField } from 'availity-reactstrap-validation';
// actions
import { createPlan, apiErrorPlan } from '../../store/plansManage/createPlans/actions';

const optionStatus = [
    {
        options: [
            { label: "0", value: 0 },
            { label: "1", value: 1 },
        ]
    },
];
const optinoReturn = [
    {
        options: [
            { label: "0", value: 0 },
            { label: "1", value: 1 },
        ]
    }
];
const CreatNewPlanComponent = ({createPlan, apiErrorPlan, CreateNewPlan, history}) => {
    const [formData, setFormData] = useState({
        name:'',
        rate:'',
        min_sum: '',
        max_sum: '',
        term_deposit: '',
        status:'',
        return_money: ''
    })
     
    const {name, rate, min_sum, max_sum, term_deposit} = formData

    const onChange = e => setFormData({...formData, [e.target.name]: e.target.value})

    function  handleValidSubmit(e) {
            createPlan(formData, history);
            console.log(formData)
    }
    const handleSelectStatus = (selectedGroup) => { setFormData({...formData, status: selectedGroup.value}); }
    const handleSelectReturnMoney = (selectedGroup) => { setFormData({...formData, return_money: selectedGroup.value}); }

    return (

        
          <React.Fragment>
          <div className="page-content">
          {CreateNewPlan.success == false ? (
              <>
              {Object.values(CreateNewPlan.error).map(v => 
                <Alert color="danger">{v}</Alert> 
                )}
              </>

          ): null}
                    <Container fluid={true}>

                        <Breadcrumbs title="Создание плана" breadcrumbItem="Администрирование" />
                        <Card>
                            <CardBody>
                            <AvForm className="form-horizontal" onValidSubmit={handleValidSubmit}>


                            <div className="form-group">
                                <AvField 
                                    name="name"
                                    value={name}
                                    onChange={e => onChange(e)}
                                    label="Имя плана" 
                                    className="form-control" 
                                    placeholder="Введите имя плана" 
                                    type="text" 
                                    required 
                                    />
                            </div>
                            

                            <div className="form-group">
                            <AvField
                                    name="rate"
                                    value={rate}
                                    onChange={e => onChange(e)}
                                    label="Рейт плана"
                                    className="form-control"
                                    placeholder="Введите рейт плана" type="number" 
                                    errorMessage="Только цифры"
                                    validate={{
                                    required: { value: true },
                                    pattern: {
                                        value: "^[0-9].+$",
                                        errorMessage: "Только цифры"
                                    }
                                    }} 
                                />
                            </div>

                            <div className="form-group">
                            <AvField
                                    name="min_sum"
                                    value={min_sum}
                                    onChange={e => onChange(e)}
                                    label="Мин. сумма"
                                    className="form-control"
                                    placeholder="Введите мин. сумму" type="number" 
                                    errorMessage="Только цифры"
                                    validate={{
                                    required: { value: true },
                                    pattern: {
                                        value: "^[0-9].+$",
                                        errorMessage: "Только цифры"
                                    }
                                    }} 
                                />
                            </div>

                            <div className="form-group">
                            <AvField
                                    name="max_sum"
                                    value={max_sum}
                                    onChange={e => onChange(e)}
                                    label="Макс. сумма"
                                    className="form-control"
                                    placeholder="Введите макс. сумму" type="number" 
                                    errorMessage="Только цифры"
                                    validate={{
                                    required: { value: true },
                                    pattern: {
                                        value: "^[0-9].+$",
                                        errorMessage: "Только цифры"
                                    }
                                    }} 
                                />
                            </div>

                            <div className="form-group">
                                <AvField
                                        name="term_deposit"
                                        value={term_deposit}
                                        onChange={e => onChange(e)}
                                        label="Депозит"
                                        className="form-control"
                                        placeholder="Введите депозит" type="number" 
                                        errorMessage="Только цифры"
                                        validate={{
                                        required: { value: true },
                                        pattern: {
                                            value: "^[0-9].+$",
                                            errorMessage: "Только цифры"
                                        }
                                        }} 
                                    />
                                </div>

                                <div className="form-group">

                                    <Label>Статус</Label>
                                    <Select
                                        options={optionStatus}
                                        onChange={e => handleSelectStatus(e)}
                                        classNamePrefix="select2-selection"
                                    />
                                </div>
                                <div className="form-group">

                                    <Label>Return</Label>
                                    <Select
                                        options={optinoReturn}
                                        onChange={e => handleSelectReturnMoney(e)}
                                        classNamePrefix="select2-selection"
                                    />
                                </div>
                            <div className="mt-3">
                                <button className="btn btn-primary btn-block waves-effect waves-light" type="submit">Создать</button>
                            </div>

                        </AvForm>
                        </CardBody>
                      </Card>
                    </Container>
                </div>
            </React.Fragment> 
          );
    }

const mapStateToProps = state => {
    return {
        CreateNewPlan: state.CreateNewPlan
    }
}
export default connect(mapStateToProps,{createPlan, apiErrorPlan})(CreatNewPlanComponent);
