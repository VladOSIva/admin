import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, CardBody, Form, FormGroup, InputGroup, Label,CardTitle, Input, Button, CardSubtitle, Alert } from "reactstrap";
import { AvForm, AvField, AvInput } from "availity-reactstrap-validation";
import "@vtaits/react-color-picker/dist/index.css";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';
// Redux 
import { connect } from 'react-redux';

import {fetchPlan, updatePlan, fetchPlanError, successUpdatePlanError, deletePlan} from '../../store/plansManage/updatePlan/actions';

const optionStatus = [
    {
        options: [
            { label: "0", value: 0 },
            { label: "1", value: 1 },
        ]
    },
];
const optinoReturn = [
    {
        options: [
            { label: "0", value: 0 },
            { label: "1", value: 1 },
        ]
    }
];

const ManagePlan = ({fetchPlan, UpdatePlan, updatePlan, history, deletePlan}) => {
	//Set id to Redux
	const planId = window.location.pathname.split('/administration/plan_update/')[1]
	useEffect(() => {
		fetchPlan(planId)
	},[])

  
  const [formData, setFormData] = useState({
		name:'',
		rate:'',
		min_sum: '',
		max_sum: '',
		term_deposit: '',
		status:'',
		return_money: ''
  })

  useEffect(() => {
	  if (UpdatePlan.fetchSuccess === true) {
		 setFormData({
                ...formData,
                name:			 UpdatePlan.fetchPlan.data.name,
                rate:			 UpdatePlan.fetchPlan.data.rate,
                min_sum: 		 UpdatePlan.fetchPlan.data.min_sum,
                max_sum: 		 UpdatePlan.fetchPlan.data.max_sum,
                term_deposit: 	 UpdatePlan.fetchPlan.data.term_deposit,
                status: 		 UpdatePlan.fetchPlan.data.status,
                return_money: 	 UpdatePlan.fetchPlan.data.return_money,
            })
		}
	},[UpdatePlan.fetchPlan.data])

	const {name, rate, min_sum, max_sum, term_deposit} = formData

	const onChange = e => setFormData({...formData, [e.target.name]: e.target.value})
	  
	const handleSelectStatus = (selectedGroup) => { setFormData({...formData, status: selectedGroup.value}); }
	const handleSelectReturnMoney = (selectedGroup) => { setFormData({...formData, return_money: selectedGroup.value}); }
	

	const Update = (e) => {
		e.preventDefault()
        updatePlan(planId, formData, history)
	}

	const Delete = (e) => {
		e.preventDefault()
		deletePlan(planId, history)
	}

    return (
    	 <React.Fragment>
				<div className="page-content">
					<Container fluid={true}>
						<Breadcrumbs title="Управление пользователями" breadcrumbItem="Администрирование" />
						<Row>
						<Col lg={12}>
						{UpdatePlan.updateSuccess == false ? (
							<>
							{Object.values(UpdatePlan.error.errors).map((v,k) => 
							  <Alert key={k} color="danger">{v}</Alert> 
							  )}
							</>
							): null}
						<Card>
						  
							<CardBody>
							  <CardTitle>Обновить/Удалить план</CardTitle>

							  <AvForm className="form-horizontal" >


									<div className="form-group">
										<AvField 
											name="name"
											value={name}
											onChange={e => onChange(e)}
											label="Имя плана" 
											className="form-control" 
											placeholder="Введите имя плана" 
											type="text" 
											required 
											/>
									</div>
									

									<div className="form-group">
									<AvField
											name="rate"
											value={rate}
											onChange={e => onChange(e)}
											label="Рейт плана"
											className="form-control"
											placeholder="Введите рейт плана" type="number" 
											errorMessage="Только цифры"
											validate={{
											required: { value: true },
											pattern: {
												value: "^[0-9].+$",
												errorMessage: "Только цифры"
											}
											}} 
										/>
									</div>

									<div className="form-group">
									<AvField
											name="min_sum"
											value={min_sum}
											onChange={e => onChange(e)}
											label="Мин. сумма"
											className="form-control"
											placeholder="Введите мин. сумму" type="number" 
											errorMessage="Только цифры"
											validate={{
											required: { value: true },
											pattern: {
												value: "^[0-9].+$",
												errorMessage: "Только цифры"
											}
											}} 
										/>
									</div>

									<div className="form-group">
									<AvField
											name="max_sum"
											value={max_sum}
											onChange={e => onChange(e)}
											label="Макс. сумма"
											className="form-control"
											placeholder="Введите макс. сумму" type="number" 
											errorMessage="Только цифры"
											validate={{
											required: { value: true },
											pattern: {
												value: "^[0-9].+$",
												errorMessage: "Только цифры"
											}
											}} 
										/>
									</div>

									<div className="form-group">
										<AvField
												name="term_deposit"
												value={term_deposit}
												onChange={e => onChange(e)}
												label="Депозит"
												className="form-control"
												placeholder="Введите депозит" type="number" 
												errorMessage="Только цифры"
												validate={{
												required: { value: true },
												pattern: {
													value: "^[0-9].+$",
													errorMessage: "Только цифры"
												}
												}} 
											/>
										</div>

										<div className="form-group">

											<Label>Статус</Label>
											<Select
												options={optionStatus}
												onChange={e => handleSelectStatus(e)}
												classNamePrefix="select2-selection"
											/>
										</div>
										<div className="form-group">

											<Label>Return</Label>
											<Select
												options={optinoReturn}
												onChange={e => handleSelectReturnMoney(e)}
												classNamePrefix="select2-selection"
											/>
										</div>

								</AvForm>
							</CardBody>
						  </Card>
						</Col>
					  </Row>
		  
						<Button 
							 color="primary"
                        	 outline 
							className="btn btn-lg btn-block waves-effect waves-light "    
							onClick={Update}
			   			>Редактировть</Button>
						<Button  
						   	color="danger"
                        	outline 
							className="btn btn-lg btn-block waves-effect waves-light "    
							onClick={Delete}
               			>Удалить</Button>
					</Container>
				</div>
			</React.Fragment>
		  );
	}
	
const mapStateToProps = state => {
	return {
		UpdatePlan: state.UpdatePlan,
	}
}

export default connect(mapStateToProps,{fetchPlan, updatePlan, fetchPlanError, successUpdatePlanError, deletePlan})(ManagePlan);
