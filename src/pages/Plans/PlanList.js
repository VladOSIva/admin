import React, { useEffect } from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col, Card, CardBody, Alert, Button } from "reactstrap";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';

// Redux 
import { connect } from 'react-redux';
//Actoins 
// import { fetchClients, errorFetch } from '../../store/clientsManage/listClients/actions';
import {fetchPlans, errorFetch } from '../../store/plansManage/listPlans/actions'
import { MDBDataTable } from "mdbreact";
// notification


const UserList = ({fetchPlans, FetchPlansList:{plans, loading}, UpdatePlan:{updateSuccess, deleteSuccess, fetchPlan}, CreateNewPlan:{plan, success}}) => {

        useEffect(() => {
            fetchPlans()
        },[fetchPlans])
        // Data fetching 
        const data = {
            columns: [
                  {
                    label: "ID",
                    field: "id",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Имя",
                    field: "name",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Рейт",
                    field: "rate",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Мин. Сумма",
                    field: "min_sum",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Макс. Сумма",
                    field: "max_sum",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Депозит",
                    field: "term_deposit",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Статус",
                    field: "status",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Return",
                    field: "return_money",
                    sort: "asc",
                    width: 150
                  },
                  {
                    label: "Действие",
                    field: "action",
                    sort: "asc",
                    width: 150
                  },
                
            ],
            rows: [
                {
                }
            ]
        }
        if (loading === false ) {
            plans.data.map(v => {
                data.rows.push({
                    id: v.id,
                    name: v.name,
                    rate: v.rate,
                    min_sum: v.min_sum,
                    max_sum: v.max_sum,
                    term_deposit: v.term_deposit,
                    status: v.status,
                    return_money: v.return_money, 
                    action: <Link to={`/administration/plan_update/${v.id}`}>Изменить</Link>
                })
            })
        }
    return (
             <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumbs */}
                        <Breadcrumbs title="Contacts" breadcrumbItem="Users List" />
                        {
                            loading !== false ? (
                                <>...loading</>
                            ) : (

                            <Row>
                            <Col lg="12">
                            <Link to='/administration/plan'>
                                <Button  color="primary"
                                outline 
                                className="btn mb-2 btn-lg btn-block waves-effect waves-light "    
                                >Создание плана</Button>
                                </Link>
                                <Card>
                                {(updateSuccess === true) ? (
                                    <div className="position-absolute mr-3"  
                                      style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="success" className="mb-0" role="alert">
                                            Пользователь:{fetchPlan.data.name} обновлен.
                                        </Alert>
                                    </div>
                                ) : null}
                                {(success === true) ? (
                                    <div className="position-absolute mr-3"  
                                      style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="success" className="mb-0" role="alert">
                                            Клиент:{plan.plan.name} создан.
                                        </Alert>
                                        </div>
                                ) : null}
                                {(deleteSuccess === true) ? (
                                    <div className="position-absolute mr-3"  
                                    style={{
                                        position: 'absolute', left: '50%', top: '5%',
                                        transform: 'translate(-50%, -50%)'
                                    }} >
                                        <Alert color="danger" className="mb-0" role="alert">
                                            Пользователь:{fetchPlan.data.name}, был удален.
                                        </Alert>
                                    </div>
                                ) : null}
                                    <CardBody>
                                         <MDBDataTable responsive bordered data={data} />
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                        )
                    }
                    </Container>
                </div>
            </React.Fragment>
          );
    }

const mapStateToProps = state => {
    return {
        FetchPlansList: state.FetchPlansList,
        UpdatePlan: state.UpdatePlan,
        CreateNewPlan: state.CreateNewPlan
    }
}
export default connect(mapStateToProps, {fetchPlans, errorFetch})(UserList);