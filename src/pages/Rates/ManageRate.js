import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, CardBody, Form, FormGroup, InputGroup, Label,CardTitle, Input, Button, CardSubtitle, Alert } from "reactstrap";
import { AvForm, AvField, AvInput } from "availity-reactstrap-validation";
import "@vtaits/react-color-picker/dist/index.css";
import "react-datepicker/dist/react-datepicker.css";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';
// Redux 
import { connect } from 'react-redux';

import { fetchRate, updateRate, fetchRateError, successUpdateRateError, deleteRate } from '../../store/ratesManage/updateRate/actions';

// Next issue, you have mistaked in the fetching store. You have written  "updateRate" - you have fetched the actions instead of dispatching store "UpdateRate"\
// Track the app logic for better clarify for yourself 

const ManageRate= ({fetchRate, UpdateRate, history, deleteRate }) => {
	//Set id to Redux
	const rateId = window.location.pathname.split('/administration/rate_update/')[1]
	useEffect(() => {
		fetchRate(rateId)
	},[])

  
  const [formData, setFormData] = useState({
	  alias:'',
      buy:'',
      sale: '',      
  })

  useEffect(() => {
	  if (UpdateRate.fetchSuccess) {
		 setFormData({
                ...formData,
                alias: UpdateRate.fetchRate.rate.alias,
                buy: UpdateRate.fetchRate.rate.buy,
                sale: UpdateRate.fetchRate.rate.sale               
            })
		}
	},[]);

  const { alias, buy, sale } = formData

  const onChange = e => setFormData({...formData, [e.target.name]: e.target.value})

	const Update = (e) => {
		e.preventDefault()
        updateRate(rateId, formData, history)
	}

	const Delete = (e) => {
		e.preventDefault()
		deleteRate(rateId, history)
	}

    return (
    	 <React.Fragment>
				<div className="page-content">
					<Container fluid={true}>
						<Breadcrumbs title="Управление пользователями" breadcrumbItem="Администрирование" />
						<Row>
						<Col lg={12}>
						{UpdateRate.updateSuccess == false ? (
							<>
							{Object.values(UpdateRate.error.errors).map((v,k) => 
							  <Alert key={k} color="danger">{v}</Alert> 
							  )}
							</>
							): null}
						<Card>
						  
							<CardBody>
							  <CardTitle>Обновить/Удалить клиента</CardTitle>

							  <AvForm>
								<AvField
								  name="alias"
								  value={alias}
								  label="Алиас"
								  placeholder="Алиас"
								  type="text"
								  onChange={e => onChange(e)}
								  errorMessage="Введите Алиас"
								  validate={{ required: { value: true } }}
                                />
                                <AvField
                                    name="buy"
                                    value={buy}
                                    label="Покупка"
                                    placeholder="Покупка"
                                    type="text"
                                    onChange={e => onChange(e)}
                                    errorMessage="Введите значение"
                                    validate={{ required: { value: true } }}
                              />
								<AvField
									name="sale"
									value={sale}
									label="Продажа"
									placeholder="Продажа"
									type="text"
									onChange={e => onChange(e)}
									errorMessage="Введите значение"
									validate={{ required: { value: true } }}
                              />                             
							  </AvForm>
							</CardBody>
						  </Card>
						</Col>
					  </Row>
		  
						<Button 
							 color="primary"
                        	 outline 
							className="btn btn-lg btn-block waves-effect waves-light "    
							onClick={Update}
			   			>Редактировть</Button>
						<Button  
						   	color="danger"
                        	outline 
							className="btn btn-lg btn-block waves-effect waves-light "    
							onClick={Delete}
               			>Удалить</Button>
					</Container>
				</div>
			</React.Fragment>
		  );
	}
	
const mapStateToProps = state => {
	return {
		UpdateRate: state.UpdateRate,
	}
}

export default connect(mapStateToProps,{fetchRate, fetchRateError, successUpdateRateError, deleteRate})(ManageRate);
