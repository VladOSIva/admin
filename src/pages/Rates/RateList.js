import React, { useEffect } from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col, Card, CardBody, Alert, Button } from "reactstrap";

//Import Breadcrumb
import Breadcrumbs from '../../components/Common/Breadcrumb';

// Redux 
import { connect } from 'react-redux';
//Actoins 
import { fetchRates, errorFetch } from '../../store/ratesManage/listRates/actions';
import { MDBDataTable } from "mdbreact";
// notification


const RateList = ({ fetchRate, fetchRates, FetchRatesList: { rates, loading }, UpdateClient: { updateSuccess, deleteSuccess, fetchClient }, CreateNewClient: { client, success } }) => {

    useEffect(() => {
        fetchRates();
    }, [fetchRates])
    // Data fetching 
    const data = {
        columns: [
            {
                label: "ID",
                field: "id",
                sort: "asc",
                width: 150
            },
            {
                label: "Алиас",
                field: "alias",
                sort: "asc",
                width: 150
            },
            {
                label: "Покупка",
                field: "buy",
                sort: "asc",
                width: 150
            },
            {
                label: "Прадажа",
                field: "sale",
                sort: "asc",
                width: 150
            },
            {
                label: "Действие",
                field: "action",
                sort: "asc",
                width: 150
            }
        ],
        rows: [
            {
            }
        ]
    }

    if (loading === false) {
        rates.data.map(v => {
            data.rows.push({
                id: v.id,
                alias: v.alias,
                buy: v.buy,
                sale: v.sale,
                action: <Link to={`/administration/rate_update/${v.id}`}>Изменить</Link>
            })
        })
    }

    return (
        <>
            <div className="page-content">
                <Container fluid>

                    {/* Render Breadcrumbs */}
                    <Breadcrumbs title="Contacts" breadcrumbItem="Users List" />
                    {
                        loading !== false ? (
                            <>...loading</>
                        ) : (

                                <Row>
                                    <Col lg="12">
                                        <Link to='/administration/rate'>
                                            <Button color="primary"
                                                outline
                                                className="btn mb-2 btn-lg btn-block waves-effect waves-light "
                                            >Создание рейта</Button>
                                        </Link>
                                        <Card>
                                            {(updateSuccess === true) ? (
                                                <div className="position-absolute mr-3"
                                                    style={{
                                                        position: 'absolute', left: '50%', top: '5%',
                                                        transform: 'translate(-50%, -50%)'
                                                    }} >
                                                    <Alert color="success" className="mb-0" role="alert">
                                                        Рейт:{fetchRate.rate.id} {fetchRate.rate.alias}, обновлен.
                                        </Alert>
                                                </div>
                                            ) : null}
                                            {(success === true) ? (
                                                <div className="position-absolute mr-3"
                                                    style={{
                                                        position: 'absolute', left: '50%', top: '5%',
                                                        transform: 'translate(-50%, -50%)'
                                                    }} >
                                                    <Alert color="success" className="mb-0" role="alert">
                                                        Клиент:{client.client.first_name} {client.client.last_name}, создан.
                                        </Alert>
                                                </div>
                                            ) : null}
                                            {(deleteSuccess === true) ? (
                                                <div className="position-absolute mr-3"
                                                    style={{
                                                        position: 'absolute', left: '50%', top: '5%',
                                                        transform: 'translate(-50%, -50%)'
                                                    }} >
                                                    <Alert color="danger" className="mb-0" role="alert">
                                                        Пользователь:{fetchClient.data.first_name} {fetchClient.data.last_name}, был удален.
                                        </Alert>
                                                </div>
                                            ) : null}
                                            <CardBody>
                                                <MDBDataTable responsive bordered data={data} />
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            )
                    }
                </Container>
            </div>
        </>
    );
}

const mapStateToProps = state => {
    return {
        FetchRatesList: state.FetchRatesList,
        UpdateClient: state.UpdateClient,
        CreateNewClient: state.CreateNewClient
    }
}
export default connect(mapStateToProps, { fetchRates, errorFetch })(RateList);