import React from "react";
import { Redirect } from "react-router-dom";

// Profile
import UserProfile from "../pages/Authentication/user-profile";

// Authentication related pages
import Login from "../pages/Authentication/Login";
import Logout from "../pages/Authentication/Logout";
import Register from "../pages/Authentication/Register";
import ForgetPwd from "../pages/Authentication/ForgetPassword";

//  // Inner Authentication
import Login1 from "../pages/AuthenticationInner/Login";
import Register1 from "../pages/AuthenticationInner/Register";
import ForgetPwd1 from "../pages/AuthenticationInner/ForgetPassword";
import LockScreen from "../pages/AuthenticationInner/auth-lock-screen";

  // Dashboard
import Dashboard from "../pages/Dashboard/index";
import DashboardSaas from "../pages/Dashboard-saas/index";
import DashboardCrypto from "../pages/Dashboard-crypto/index";

// Administraiton Users
import UserList from '../pages/Administration/UserList';
import CreateUser from '../pages/Administration/CreateUser';
import ManageUser from '../pages/Administration/ManageUser';
// Administration Clients
import ClientList from '../pages/Cients/ClientList';
import CreateClient from '../pages/Cients/CreateClient';
import ManageClient from '../pages/Cients/ManageClient';
// Administration Plans
import PlanList from '../pages/Plans/PlanList';
import CreatePlan from '../pages/Plans/CreatePlan';
import ManagePlan from '../pages/Plans/ManagePlan';
// Administration Rates
import RateList from '../pages/Rates/RateList';
import CreateRate from '../pages/Rates/CreateRate';
import ManageRate from '../pages/Rates/ManageRate';

const userRoutes = [
	//Administration users
	{ path: '/administration/users/', component: UserList },
	{ path: '/administration/user', component: CreateUser },
	{ path: '/administration/user_update/:id', component: ManageUser},
	//Administration clients
	{ path: '/administration/clients', component: ClientList},
	{ path: '/administration/client', component: CreateClient},
	{ path: '/administration/client_update/:id', component: ManageClient},
	//Administration plans
	{ path: '/administration/plans', component: PlanList},
	{ path: '/administration/plan', component: CreatePlan},
	{ path: '/administration/plan_update/:id', component: ManagePlan},
	//Administration retes
	{ path: '/administration/rates', component: RateList},
	{ path: '/administration/rate', component: CreateRate},
	{ path: '/administration/rate_update/:id', component: ManageRate},

	
	{ path: "/dashboard", component: Dashboard },
	{ path: "/dashboard-saas", component: DashboardSaas },
	{ path: "/dashboard-crypto", component: DashboardCrypto },

	// //profile
	{ path: "/profile", component: UserProfile },
	
	// this route should be at the end of all other routes
	{ path: "/", exact: true, component: () => <Redirect to="/dashboard" /> }
];

const authRoutes = [
	{ path: "/logout", component: Logout },
	{ path: "/login", component: Login },
	{ path: "/forgot-password", component: ForgetPwd },
	{ path: "/register", component: Register },

	// Authentication Inner
	{ path: "/pages-login", component: Login1 },
	{ path: "/pages-register", component: Register1 },
	{ path: "/pages-forget-pwd", component: ForgetPwd1 },
	{ path : "/auth-lock-screen", component: LockScreen }
];

export { userRoutes, authRoutes };
