import { LOGIN_USER, LOGIN_SUCCESS, LOGOUT_USER, LOGOUT_USER_SUCCESS, API_ERROR, REMOVE_API_ERROR } from './actionTypes';

const initialState = {
    errors: "",
    loading: false,
    success: false
}

const login = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER:
            state = {
                ...state,
                loading: true
            }
            break;
        case LOGIN_SUCCESS:
            state = {
                ...state,
                loading: false,
                success: true
            }
            break;
        case LOGOUT_USER:
            state = { ...state };
            break;
        case LOGOUT_USER_SUCCESS:
            state = { ...state };
            break;
        case API_ERROR:
            state = { ...state, errors: action.payload, loading: false };
            break;
        case REMOVE_API_ERROR:
            state = {...state, errors:'', loading: false}
            break;
        default:
            state = { ...state };
            break;
    }
    return state;
}

export default login;