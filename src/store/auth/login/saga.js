import { takeEvery, fork, put, all, call } from 'redux-saga/effects';
// Login Redux States
import { LOGIN_SUCCESS, LOGIN_USER, LOGOUT_USER } from './actionTypes';
import { loginSuccess, logoutUserSuccess, apiError, removeApiError } from './actions';

import axios from 'axios';

// const fireBaseBackend = getFirebaseBackend();
const delay = time => new Promise(resolve => setTimeout(resolve, time));

async function loginData (url,{email, password}) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify({email,password});
    try { 
    const response = await axios.post(url, body, config);
    return  await response.data
    } catch(e) {
        throw e.response.data
    }
}


function* loginUser({ payload: { user, history } }) {
    try {
        
           const response = yield call(loginData, `${process.env.REACT_APP_BACKEND}/api/auth/login`, {email: user.email, password: user.password});
           localStorage.setItem("authUser", JSON.stringify(response));
           yield put(loginSuccess(response));
        
           history.push('/dashboard');
          
    } catch (e) {
        
        yield put(apiError(e));
        yield call(delay, 1000)
        yield put(removeApiError());
            
    }
}

function* logoutUser({ payload: { history } }) {
    try {
         localStorage.removeItem("authUser");

        //  if(process.env.REACT_APP_DEFAULTAUTH === 'firebase')
        //  {
        //    const response = yield call(fireBaseBackend.logout);
        //    yield put(logoutUserSuccess(response));
        //  }
        history.push('/login');
    } catch (error) {
        yield put(apiError(error));
    }
}


export function* watchUserLogin() {
    yield takeEvery(LOGIN_USER, loginUser)
}

export function* watchUserLogout() {
    yield takeEvery(LOGOUT_USER, logoutUser)
}

function* authSaga() {
    yield all([
        fork(watchUserLogin),
        fork(watchUserLogout)
    ]);
}

export default authSaga;