export const CREATE_CLIENT         = 'CREATE_CLIENT';
export const SUCCESS_CREATE_CLIENT     = 'SUCCESS_CREATE_CLIENT';
export const FAIL_CREATE_CLIENT        = 'FAIL_CREATE_CLIENT';
export const REMOVE_FAIL_CREATE_CLIENT = 'REMOVE_FAIL_CREATE_CLIENT';
export const REMOVE_SUCCESS_CREATE_BANNER = 'REMOVE_SUCCESS_CREATE_BANNER'; 