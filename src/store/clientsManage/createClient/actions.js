import {
    CREATE_CLIENT,
    SUCCESS_CREATE_CLIENT, 
    FAIL_CREATE_CLIENT,
    REMOVE_FAIL_CREATE_CLIENT,
    REMOVE_SUCCESS_CREATE_BANNER
} from './actionTypes';

export const createClient = (client, history) => {
    return {
        type: CREATE_CLIENT,
        payload:{client, history},
        success: false
    }
}
export const successCreateClient = (client) => {
    return {
        type: SUCCESS_CREATE_CLIENT,
        payload: client,
        success: true
    }
}
export const removeCreateSuccessBanner = () => {
    return {
        type: REMOVE_SUCCESS_CREATE_BANNER,
        success: false
    }
}
export const apiErrorClient = (error) => {
    return {
        type: FAIL_CREATE_CLIENT,
        payload: error
    }
}
export const removeApiErrorClient = () => {
    return {
        type: REMOVE_FAIL_CREATE_CLIENT
    }
}