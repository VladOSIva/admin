
import {
    SUCCESS_CREATE_CLIENT, 
    FAIL_CREATE_CLIENT,
    REMOVE_FAIL_CREATE_CLIENT,
    REMOVE_SUCCESS_CREATE_BANNER
} from './actionTypes';

const initialState = {
    error:"",
    success: null,
    client: []
}


const createClient = (state = initialState, action) => {

    switch(action.type) {
        case SUCCESS_CREATE_CLIENT:
            return {
                ...state,
                success: true,
                client: action.payload
            };
        case FAIL_CREATE_CLIENT:
            return {
                ...state,
                success: false,
                error: action.payload,
            };
        case REMOVE_SUCCESS_CREATE_BANNER:
            return {
                ...state,
                success: false, 
                error: ""
            };
        case REMOVE_FAIL_CREATE_CLIENT:
            return {
                ...state,
                success: null
            };
        default:
            return {
                ...state
            }
    }
};
export default createClient;