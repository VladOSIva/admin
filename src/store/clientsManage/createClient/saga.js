import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// FetchUser Redux States 
import { CREATE_CLIENT } from './actionTypes';
import { successCreateClient, apiErrorClient, removeApiErrorClient, removeCreateSuccessBanner} from './actions';
import axios from 'axios'; 
// delay action with calling
const delay = time => new Promise(resolve => setTimeout(resolve, time));

async function createNewClient(url,{first_name, last_name, email, login, phone, password, confirm_password}) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
     }
    const body = JSON.stringify({first_name, last_name, email, login, phone, password, confirm_password});
    try { 
    const response = await axios.post(url, body, config);
    return  await response.data
    } catch(e) {
        throw e.response.data
    }
};
function* createClient({ payload: { client, history } }) {
    try {
        const response = yield (call(createNewClient, `${process.env.REACT_APP_BACKEND}/api/clients`,
         {
             first_name: client.first_name,
             last_name: client.last_name,
             email: client.email,
             login: client.login,
             phone: client.phone,
             password: client.password,
             confirm_password: client.confirm_password
        }))  

        yield put(successCreateClient(response))
        history.push('/administration/clients/')
        yield call(delay, 4000)
        yield put(removeCreateSuccessBanner())

    } catch(e) {
        console.log(e)
        yield put(apiErrorClient(e.errors));
        yield call(delay, 1000)
        yield put(removeApiErrorClient());
      
    }
}

export function* watchCreateClient() {
    yield takeEvery(CREATE_CLIENT, createClient)
}
function* CreateClientSaga() {
    yield all([
        fork(watchCreateClient)
    ])
}

export default CreateClientSaga