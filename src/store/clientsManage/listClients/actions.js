import {
    FETCH_ERROR,
    FETCH_SUCCESS,
    FETCH_CLIENTS,
} from './actionTypes';

export const fetchClients = (clients, history) => {
    return { 
        type: FETCH_CLIENTS,
        payload: {clients, history}
    }
};
export const fetchSuccess = (clients) => {
    return {
        type: FETCH_SUCCESS,
        payload: clients
    }
}
export const errorFetch = (error) => {
    return {
        type: FETCH_ERROR,
        payload: error
    }
}
