import {
    FETCH_ERROR,
    FETCH_SUCCESS,
    FETCH_CLIENTS,
} from './actionTypes';

const initialState = {
    error: "",
    loading: null, 
    clients: [],
};

const fetchClientsList = (state = initialState, action) => {

    switch(action.type) {
        case FETCH_CLIENTS:
            return {
                ...state, 
                loading: true, 
            };
        case FETCH_SUCCESS:
            return {
                ...state,
                loading: false,
                clients: action.payload,
            };
        case FETCH_ERROR:
            return {
                ...state,
                error: action.payload, loading:true
            };
        default:
            return {
                ...state
            }
    }
};
export default fetchClientsList