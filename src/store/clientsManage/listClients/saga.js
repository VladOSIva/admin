import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// FetchUser Redux States 
import { FETCH_CLIENTS } from './actionTypes';
import { fetchSuccess, errorFetch, } from './actions';
import axios from 'axios';

async function fetchClientsList (url) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };
    try {
        const response = await axios.get(url,config)
        return await response.data
    } catch(e) {
        throw e
    }
};
function* fetchList() {
    try {
        const response = yield call(fetchClientsList, `${process.env.REACT_APP_BACKEND}/api/clients/`);
        yield put(fetchSuccess(response))
        
    } catch (e) {
        yield put(errorFetch('Ошибка повторите снова'));
    }
};
export function* watchClientsFetching() {
    yield takeEvery(FETCH_CLIENTS, fetchList)
};

function* ClientsFetchSaga() {
    yield all([
        fork(watchClientsFetching)
    ]) 
};

export default ClientsFetchSaga;