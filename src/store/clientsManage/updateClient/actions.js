import {
    FETCH_CLIENT_INFO,       
    ERROR_FETCH_CLIENT_INFO, 
    DELETE_CLIENT_SUCCESS,
    FETCH_SUCCESS_CLIENT,
    DELETE_CLIENT,
    CLIENT_UPDATE,
    ERROR_DELETE_CLIENT,
    SUCCESS_UPDATE_CLIENT,
    ERROR_SUCCESS_UPDATE_CLIENT,
    REMOVE_ERROR_SUCCESS_UPDATE_CLIENT,
    REMOVE_SUCCESS_BANNER_UPDATE_CLIENT,
    REMOVE_DELETE_SUCCESS_BANNER_CLIENT
} from './actionTypes';

export const fetchClient = (clientId) => {
    return {
        type: FETCH_CLIENT_INFO,
        payload: clientId,
        fetchSuccess: false
    }
}
export const fetchClientSuccess = (client) => {
        return {
            type: FETCH_SUCCESS_CLIENT,
            payload:client,
            fetchSuccess: true
        }
}
export const fetchClientError = (error) => {
    return {
        type: ERROR_FETCH_CLIENT_INFO,
        payload: error
    }
}
export const deleteClient = (clientId, history) => {
    return {
        type: DELETE_CLIENT,
        payload:{clientId, history},
        deleteSuccess: false
    }
}
export const deleteClientSuccess = (client) => {
    return {
        type: DELETE_CLIENT_SUCCESS,
        payload: client,
        deleteSuccess: true
    }
}
export const deleteClientError = (error) => {
    return {
        type: ERROR_DELETE_CLIENT,
        deleteSuccess: false,
        error: error
    }
}

export const updateClient = (clientId, client, history) => {
    return {
        type: CLIENT_UPDATE,
        payload: {clientId, client, history},
        fetchSuccess: false
    }
}
export const successUpdateClient = (client) => {
    return {
        type: SUCCESS_UPDATE_CLIENT,
        payload: client,
        updateSuccess: true
    }
}
export const removeSuccessBannerUpdateClient = () => {
    return {
        type: REMOVE_SUCCESS_BANNER_UPDATE_CLIENT
    }
}
export const successUpdateClientError = (error) => {
    return {
        type: ERROR_SUCCESS_UPDATE_CLIENT,
        payload: error,
        updateSuccess: false
    }
}
export const removeSuccessUpdateClientError = () => {
    return {
        type: REMOVE_ERROR_SUCCESS_UPDATE_CLIENT,
    }
}
export const removeSuccessDeleteBannerClient = () => {
    return {
        type: REMOVE_DELETE_SUCCESS_BANNER_CLIENT,
    }
}