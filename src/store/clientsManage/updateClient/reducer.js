import {
    FETCH_CLIENT_INFO,       
    ERROR_FETCH_CLIENT_INFO, 
    DELETE_CLIENT_SUCCESS,
    FETCH_SUCCESS_CLIENT,
    ERROR_DELETE_CLIENT,
    SUCCESS_UPDATE_CLIENT,
    ERROR_SUCCESS_UPDATE_CLIENT,
    REMOVE_ERROR_SUCCESS_UPDATE_CLIENT,
    REMOVE_SUCCESS_BANNER_UPDATE_CLIENT,
    REMOVE_DELETE_SUCCESS_BANNER_CLIENT
} from './actionTypes';

const initialState = {
    error: "",
    updateSuccess: null,
    deleteSuccess: null,
    fetchSuccess: null,
    clientId: null,
    fetchClient: []
};

const updateClient = (state = initialState, action) => {

    switch(action.type) {
        case FETCH_CLIENT_INFO:
            return {
                ...state,
                fetchSuccess: false
            };
        case FETCH_SUCCESS_CLIENT:
            return {
                ...state,
                fetchSuccess: true,
                fetchClient: action.payload
            }
        case ERROR_FETCH_CLIENT_INFO:
            return {
                ...state,
                fetchSuccess: false,
                error: action.payload,
            }
        case DELETE_CLIENT_SUCCESS:
            return {
                ...state,
                deleteSuccess: true
            };
        case ERROR_DELETE_CLIENT:
            return {
                ...state,
                deleteSuccess: false,
                error: action.payload
            };
        case REMOVE_DELETE_SUCCESS_BANNER_CLIENT:
            return {
                ...state,
                deleteSuccess: null
            }
        case SUCCESS_UPDATE_CLIENT:
            return {
                ...state,
                updateSuccess: true
            };
        case ERROR_SUCCESS_UPDATE_CLIENT:
            return {
                ...state,
                updateSuccess: false,
                error: action.payload
            };
        case REMOVE_ERROR_SUCCESS_UPDATE_CLIENT:
            return {
                ...state,
                updateSuccess: null,
                error: null
            };
        case REMOVE_SUCCESS_BANNER_UPDATE_CLIENT:
            return {
                ...state,
                updateSuccess: null,
            };
        default:
            return {
                ...state
            }

    }
} 
export default updateClient;