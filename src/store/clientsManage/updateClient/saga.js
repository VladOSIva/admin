import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// Update/Delete user 
import {
    FETCH_CLIENT_INFO,       
    CLIENT_UPDATE,
    DELETE_CLIENT
} from './actionTypes';

import axios from 'axios';

import {
    fetchClientError, 
    fetchClientSuccess,
    deleteClientSuccess,
    deleteClientError,
    removeSuccessDeleteBannerClient,
    successUpdateClient,
    successUpdateClientError,
    removeSuccessUpdateClientError,
    removeSuccessBannerUpdateClient
} from './actions';

const delay = time => new Promise(resolve => setTimeout(resolve, time));

async function fetchClientInfo(urlId) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    }
    try {
        const response = await axios.get(urlId, config)
        return await response.data
    } catch(e) {
        throw e.response.data
    }
}
async function clientUpdateInfo(urlId,{name,email,password, confirm_password, permission}) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    }
    const body = JSON.stringify({name,email,password, confirm_password,permission});
    try {
        
        const response = await axios.put(urlId, body, config)
        return await response.data
    } catch(e) {
        throw e.response.data
    }
}
async function clientDeleteReq(urlId) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    }
    try {
        
        const response = await axios.delete(urlId, config)
        return await response.data
    } catch(e) {
        throw e.response.data
    }
}

function* clientDelete({payload}) {

    try { 
        const response = yield (call(clientDeleteReq, `${process.env.REACT_APP_BACKEND}/api/clients/${payload.clientId}`))
        yield put(deleteClientSuccess(response))
        payload.history.push('/administration/clients/')
        yield call(delay, 2000)
        yield put(removeSuccessDeleteBannerClient())
    } catch(e) {
        yield put(deleteClientError(e))
    }
}

function* updateClient ({payload}) {

    try { 
        const response = yield (call(clientUpdateInfo, `${process.env.REACT_APP_BACKEND}/api/clients/${payload.clientId}`,
         {   first_name: payload.client.first_name,
             last_name: payload.client.last_name,
             email: payload.client.email,
             login: payload.client.login,
             phone: payload.client.phone,
             password: payload.client.password,
             confirm_password: payload.client.confirm_password}))
            yield put(successUpdateClient(response))
            payload.history.push('/administration/clients/')
            yield call(delay, 2000)
            yield put(removeSuccessBannerUpdateClient())
        
    } catch(e) {
        yield put(successUpdateClientError(e))
        yield call(delay, 2000)
        yield put(removeSuccessUpdateClientError())
    }
}

function* fetchClientUpdate({payload}) {

    try { 
        const response = yield (call(fetchClientInfo, `${process.env.REACT_APP_BACKEND}/api/clients/${payload}`))
        yield put(fetchClientSuccess(response))

    } catch(e) {
        yield put(fetchClientError(e))
    }
}

export function* watchFetchClientUpdate() {
    yield takeEvery(FETCH_CLIENT_INFO, fetchClientUpdate)
}
export function* watchClientUpdate() {
    yield takeEvery(CLIENT_UPDATE, updateClient)
}
export function* watchClientDelete() {
    yield takeEvery(DELETE_CLIENT, clientDelete)
}
function* UpdateClientSaga() {
    yield all([
        fork(watchFetchClientUpdate),
        fork(watchClientUpdate),
        fork(watchClientDelete)
    ])
}
export default UpdateClientSaga