// Create new plan
export const CREATE_PLAN                  = 'CREATE_PLAN';
export const SUCCESS_CREATE_PLAN          = 'SUCCESS_CREATE_PLAN';
export const FAIL_CREATE_PLAN             = 'FAIL_CREATE_PLAN';
export const REMOVE_FAIL_CREATE_PLAN      = 'REMOVE_FAIL_CREATE_PLAN';
export const REMOVE_SUCCESS_CREATE_BANNER = 'REMOVE_SUCCESS_CREATE_BANNER'; 