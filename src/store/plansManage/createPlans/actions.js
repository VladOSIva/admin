import {  
    CREATE_PLAN,                 
    SUCCESS_CREATE_PLAN,         
    FAIL_CREATE_PLAN,            
    REMOVE_FAIL_CREATE_PLAN,     
    REMOVE_SUCCESS_CREATE_BANNER
} from "./actionTypes";

export const createPlan = (plan, history) => {
    return {
        type: CREATE_PLAN,
        payload:{plan, history},
        success: false
    }
}
export const successCreatePlan = (plan) => {
    return {
        type: SUCCESS_CREATE_PLAN,
        payload: plan,
        success: true
    }
}
export const removeCreateSuccessBanner = () => {
    return {
        type: REMOVE_SUCCESS_CREATE_BANNER,
        success: false
    }
}
export const apiErrorPlan = (error) => {
    return {
        type: FAIL_CREATE_PLAN,
        payload: error
    }
}
export const removeApiErrorPlan = () => {
    return {
        type: REMOVE_FAIL_CREATE_PLAN
    }
}