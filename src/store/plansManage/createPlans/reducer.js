import {
    SUCCESS_CREATE_PLAN,         
    FAIL_CREATE_PLAN,            
    REMOVE_FAIL_CREATE_PLAN,     
    REMOVE_SUCCESS_CREATE_BANNER
} from './actionTypes';

const initialState = {
    error:"",
    success: null,
    plan: []
}


const createPlan = (state = initialState, action) => {

    switch(action.type) {
        case SUCCESS_CREATE_PLAN:
            return {
                ...state,
                success: true,
                plan: action.payload
            };
        case FAIL_CREATE_PLAN:
            return {
                ...state,
                success: false,
                error: action.payload,
            };
        case REMOVE_SUCCESS_CREATE_BANNER:
            return {
                ...state,
                success: false, 
                error: ""
            };
        case REMOVE_FAIL_CREATE_PLAN:
            return {
                ...state,
                success: null
            };
        default:
            return {
                ...state
            }
    }
};
export default createPlan;