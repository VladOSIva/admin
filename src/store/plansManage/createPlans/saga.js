import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// FetchUser Redux States 
import { CREATE_PLAN } from './actionTypes';
import { successCreatePlan, apiErrorPlan, removeApiErrorPlan, removeCreateSuccessBanner} from './actions';
import axios from 'axios'; 
// delay action with calling
const delay = time => new Promise(resolve => setTimeout(resolve, time));

async function createNewPlan(url,{name, rate, min_sum, max_sum, term_deposit, status, return_money}) {

    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
     };

    const body = JSON.stringify({name, rate, min_sum, max_sum, term_deposit, status, return_money});

    try { 

    const response = await axios.post(url, body, config);
    return  await response.data;

    } catch(e) {

        throw e.response.data;

    }
};

function* createPlan({ payload: { plan, history } }) {
    try {
        console.log(plan)
        const response = yield (call(createNewPlan, `${process.env.REACT_APP_BACKEND}/api/plans`,
         {
            name: plan.name,
            rate: plan.rate,
            min_sum: plan.min_sum,
            max_sum: plan.max_sum,
            term_deposit: plan.term_deposit,
            status: plan.status,
            return_money: plan.return_money
        }));

        yield put(successCreatePlan(response));
        history.push('/administration/plans/');
        yield call(delay, 4000);
        yield put(removeCreateSuccessBanner());

    } catch(e) {
        console.log(e)
        yield put(apiErrorPlan(e.errors));
        yield call(delay, 1000);
        yield put(removeApiErrorPlan());
      
    }
};

export function* watchCreatePlan() {

    yield takeEvery(CREATE_PLAN, createPlan)

};

function* CreatePlanSaga() {

    yield all([
        fork(watchCreatePlan)
    ]);

};

export default CreatePlanSaga;