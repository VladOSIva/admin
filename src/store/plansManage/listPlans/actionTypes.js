// Types for fetching plans
export const FETCH_PLANS = 'FETCH_PLANS';
export const FETCH_PLANS_SUCCESS = 'FETCH_PLANS_SUCCESS';
export const FETCH_PLANS_ERROR  = 'FETCH_PLANS_ERROR';