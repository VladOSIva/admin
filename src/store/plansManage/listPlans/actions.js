import { 
 FETCH_PLANS,
 FETCH_PLANS_SUCCESS,
 FETCH_PLANS_ERROR  
} from './actionTypes'

export const fetchPlans = (plans, history) => {
    return {
        type: FETCH_PLANS,
        payload: {plans, history}
    }
}
export const fetchSuccess = (plans) => {
    return {
        type: FETCH_PLANS_SUCCESS,
        payload: plans
    }
}
export const errorFetch = (error) => {
    return {
        type: FETCH_PLANS_ERROR,
        payload: error
    }
}