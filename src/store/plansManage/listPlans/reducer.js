import { 
    FETCH_PLANS,
    FETCH_PLANS_SUCCESS,
    FETCH_PLANS_ERROR  
   } from './actionTypes'

const initialState = {
    error: "",
    loading: null,
    plans: []
}

const fetchPlansList = (state = initialState, action) => {

    switch(action.type) {
        case FETCH_PLANS:
            return {
                ...state,
                loading: true 
            };
        case FETCH_PLANS_SUCCESS:
            return {
                ...state, 
                loading: false,
                plans: action.payload
            };
        case FETCH_PLANS_ERROR:
            return {
                ...state,

            };
        default: 
            return {
                ...state
            }
    }
}

export default fetchPlansList