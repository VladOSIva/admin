import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// FetchUser Redux States 
import { FETCH_PLANS } from './actionTypes';
import { fetchSuccess, errorFetch } from './actions';
import axios from 'axios';

async function fetchPlansList (url) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };
    try {
        const response = await axios.get(url,config)
        return await response.data
    } catch(e) {
        throw e
    }
};

function* fetchList() {
    try {
        const response = yield call(fetchPlansList, `${process.env.REACT_APP_BACKEND}/api/plans/`);
        yield put(fetchSuccess(response))
        
    } catch (e) {
        yield put(errorFetch('Ошибка повторите снова'));
    }
};
export function* watchPlansFetching() {
    yield takeEvery(FETCH_PLANS, fetchList)
};
function* PlansFetchSaga() {
    yield all([
        fork(watchPlansFetching)
    ]) 
};

export default PlansFetchSaga;