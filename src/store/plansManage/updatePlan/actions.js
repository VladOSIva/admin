import {
    FETCH_PLAN_INFO,       
    ERROR_FETCH_PLAN_INFO, 
    DELETE_PLAN_SUCCESS,
    FETCH_SUCCESS_PLAN,
    DELETE_PLAN,
    PLAN_UPDATE,
    ERROR_DELETE_PLAN,
    SUCCESS_UPDATE_PLAN,
    ERROR_SUCCESS_UPDATE_PLAN,
    REMOVE_ERROR_SUCCESS_UPDATE_PLAN,
    REMOVE_DELETE_SUCCESS_BANNER_PLAN,
    REMOVE_SUCCESS_BANNER_UPDATE_PLAN
} from './actionTypes';

export const fetchPlan = (planId) => {
    return {
        type: FETCH_PLAN_INFO,
        payload: planId,
        fetchSuccess: false
    }
}
export const fetchPlanSuccess = (plan) => {
        return {
            type: FETCH_SUCCESS_PLAN,
            payload:plan,
            fetchSuccess: true
        }
}
export const fetchPlanError = (error) => {
    return {
        type: ERROR_FETCH_PLAN_INFO,
        payload: error
    }
}
export const deletePlan = (planId, history) => {
    return {
        type: DELETE_PLAN,
        payload:{planId, history},
        deleteSuccess: false
    }
}
export const deletePlanSuccess = (plan) => {
    return {
        type: DELETE_PLAN_SUCCESS,
        payload: plan,
        deleteSuccess: true
    }
}
export const deletePlanError = (error) => {
    return {
        type: ERROR_DELETE_PLAN,
        deleteSuccess: false,
        error: error
    }
}

export const updatePlan = (planId, plan, history) => {
    return {
        type: PLAN_UPDATE,
        payload: {planId, plan, history},
        fetchSuccess: false
    }
}
export const successUpdatePlan = (plan) => {
    return {
        type: SUCCESS_UPDATE_PLAN,
        payload: plan,
        updateSuccess: true
    }
}
export const removeSuccessBannerUpdatePlan = () => {
    return {
        type: REMOVE_SUCCESS_BANNER_UPDATE_PLAN
    }
}
export const successUpdatePlanError = (error) => {
    return {
        type: ERROR_SUCCESS_UPDATE_PLAN,
        payload: error,
        updateSuccess: false
    }
}
export const removeSuccessUpdatePlanError = () => {
    return {
        type: REMOVE_ERROR_SUCCESS_UPDATE_PLAN,
    }
}
export const removeSuccessDeleteBannerPlan = () => {
    return {
        type: REMOVE_DELETE_SUCCESS_BANNER_PLAN,
    }
}