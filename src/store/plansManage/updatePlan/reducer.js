import {
    FETCH_PLAN_INFO,       
    ERROR_FETCH_PLAN_INFO, 
    DELETE_PLAN_SUCCESS,
    FETCH_SUCCESS_PLAN,
    ERROR_DELETE_PLAN,
    SUCCESS_UPDATE_PLAN,
    ERROR_SUCCESS_UPDATE_PLAN,
    REMOVE_ERROR_SUCCESS_UPDATE_PLAN,
    REMOVE_DELETE_SUCCESS_BANNER_PLAN,
    REMOVE_SUCCESS_BANNER_UPDATE_PLAN
} from './actionTypes';

const initialState = {
    error: "",
    updateSuccess: null,
    deleteSuccess: null,
    fetchSuccess: null,
    planId: null,
    fetchPlan: []
};

const updatePlan = (state = initialState, action) => {

    switch(action.type) {
        case FETCH_PLAN_INFO:
            return {
                ...state,
                fetchSuccess: false
            };
        case FETCH_SUCCESS_PLAN:
            return {
                ...state,
                fetchSuccess: true,
                fetchPlan: action.payload
            }
        case ERROR_FETCH_PLAN_INFO:
            return {
                ...state,
                fetchSuccess: false,
                error: action.payload,
            }
        case DELETE_PLAN_SUCCESS:
            return {
                ...state,
                deleteSuccess: true
            };
        case ERROR_DELETE_PLAN:
            return {
                ...state,
                deleteSuccess: false,
                error: action.payload
            };
        case REMOVE_DELETE_SUCCESS_BANNER_PLAN:
            return {
                ...state,
                deleteSuccess: null
            }
        case SUCCESS_UPDATE_PLAN:
            return {
                ...state,
                updateSuccess: true
            };
        case ERROR_SUCCESS_UPDATE_PLAN:
            return {
                ...state,
                updateSuccess: false,
                error: action.payload
            };
        case REMOVE_ERROR_SUCCESS_UPDATE_PLAN:
            return {
                ...state,
                updateSuccess: null,
                error: null
            };
        case REMOVE_SUCCESS_BANNER_UPDATE_PLAN:
            return {
                ...state,
                updateSuccess: null,
            };
        default:
            return {
                ...state
            }

    }
} 
export default updatePlan;