import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// Update/Delete user 
import {
    FETCH_PLAN_INFO,       
    PLAN_UPDATE,
    DELETE_PLAN
} from './actionTypes';

import axios from 'axios';

import {
    fetchPlanError,
    fetchPlanSuccess,
    deletePlanSuccess,
    deletePlanError,
    removeSuccessDeleteBannerPlan,
    successUpdatePlan,
    successUpdatePlanError,
    removeSuccessUpdatePlanError,
    removeSuccessBannerUpdatePlan
} from './actions';

const delay = time => new Promise(resolve => setTimeout(resolve, time));

async function fetchPlanInfo(urlId) {
    
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };

    try {

        const response = await axios.get(urlId, config);
        return await response.data

    } catch(e) {

        throw e.response.data

    }
};
async function planUpdateInfo(urlId,{name,email,password, confirm_password, permission}) {

    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };

    const body = JSON.stringify({name,email,password, confirm_password,permission});
    
    try {
        
        const response = await axios.put(urlId, body, config);
        return await response.data
    
    } catch(e) {
    
        throw e.response.data
    
    }
};

async function planDeleteReq(urlId) {
    
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };
    
    try {
        
        const response = await axios.delete(urlId, config);
        return await response.data
    
    } catch(e) {
    
        throw e.response.data
    
    }
};

function* planDelete({payload: {planId, history}}) {

    try { 

        const response = yield (call(planDeleteReq, `${process.env.REACT_APP_BACKEND}/api/plans/${planId}`));
        yield put(deletePlanSuccess(response));
        history.push('/administration/plans/');
        yield call(delay, 2000);
        yield put(removeSuccessDeleteBannerPlan());

    } catch(e) {

        yield put(deletePlanError(e));

    }
};

function* updatePlan ({payload: {plan, history, planId}}) {

    try { 
        const response = yield (call(planUpdateInfo, `${process.env.REACT_APP_BACKEND}/api/plans/${planId}`,
            {   
                name: plan.name,
                rate: plan.rate,
                min_sum: plan.min_sum,
                max_sum: plan.max_sum,
                term_deposit: plan.term_deposit,
                status: plan.status
            }));

            yield put(successUpdatePlan(response));
            history.push('/administration/plans/');
            yield call(delay, 2000);
            yield put(removeSuccessBannerUpdatePlan());
        
    } catch(e) {
        yield put(successUpdatePlanError(e));
        yield call(delay, 2000);
        yield put(removeSuccessUpdatePlanError());
    }
};

function* fetchPlanUpdate({payload}) {

    try { 

        const response = yield (call(fetchPlanInfo, `${process.env.REACT_APP_BACKEND}/api/plans/${payload}`))
        yield put(fetchPlanSuccess(response))

    } catch(e) {
        yield put(fetchPlanError(e))
    }
};

export function* watchFetchPlanUpdate() {
    yield takeEvery(FETCH_PLAN_INFO, fetchPlanUpdate)
};

export function* watchPlanUpdate() {
    yield takeEvery(PLAN_UPDATE, updatePlan)
};

export function* watchPlanDelete() {
    yield takeEvery(DELETE_PLAN, planDelete)
};

function* UpdatePlanSaga() {
    yield all([
        fork(watchFetchPlanUpdate),
        fork(watchPlanUpdate),
        fork(watchPlanDelete)
    ])
};

export default UpdatePlanSaga