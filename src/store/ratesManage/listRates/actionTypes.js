// Types for fetching all users that admin has submited 
export const FETCH_RATES         = 'FETCH_RATES';
export const FETCH_SUCCESS_RATES = 'FETCH_SUCCESS_RATES';
export const FETCH_ERROR_RATES   = 'FETCH_ERROR_RATES';