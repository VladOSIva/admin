import {
    FETCH_RATES,        
    FETCH_SUCCESS_RATES,
    FETCH_ERROR_RATES,  
} from './actionTypes';

export const fetchRates = () => {
    return { 
        type: FETCH_RATES,        
    }
};
export const fetchSuccess = (rates) => {
    return {
        type: FETCH_SUCCESS_RATES,
        payload: rates
    }
}
export const errorFetch = (error) => {
    return {
        type: FETCH_ERROR_RATES,
        payload: error
    }
}

