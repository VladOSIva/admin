import {
    FETCH_RATES,        
    FETCH_SUCCESS_RATES,
    FETCH_ERROR_RATES,  
} from './actionTypes';

const initialState = {
    error: "",
    loading: null, 
    rates: [],
};

const fetchRatesList = (state = initialState, action) => {

    switch(action.type) {
        case FETCH_RATES:
            return {
                ...state, 
                loading: true, 
            };
        case FETCH_SUCCESS_RATES:
            return {
                ...state,
                loading: false,
                rates: action.payload,
            };
        case FETCH_ERROR_RATES:
            return {
                ...state,
                error: action.payload, 
                loading:true
            };
        default:
            return {
                ...state
            }
    }
};
export default fetchRatesList;