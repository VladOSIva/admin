import { takeEvery, fork, put, all, call } from 'redux-saga/effects';
// FetchUser Redux States 
import { FETCH_RATES } from './actionTypes';
import { fetchSuccess, errorFetch } from './actions';
import axios from 'axios';

async function fetchRatesList (url) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };
    try {
        const response = await axios.get(url,config)
        return await response.data
    } catch(e) {
        throw e
    }
};

function* fetchList() {
    try {
        const response = yield call(fetchRatesList, `${process.env.REACT_APP_BACKEND}/api/rates/`);
        yield put(fetchSuccess(response))
        
    } catch (e) {
        yield put(errorFetch('Ошибка, повторите снова'));
    }
};
export function* watchRatesFetching() {
    yield takeEvery(FETCH_RATES, fetchList)
};

function* RatesFetchSaga() {
    yield all([
        fork(watchRatesFetching)
    ]) 
};

export default RatesFetchSaga;