// Action types for update

export const FETCH_RATE_INFO                   = 'FETCH_RATE_INFO';
export const ERROR_FETCH_RATE_INFO             = 'ERROR_FETCH_RATE_INFO';
export const FETCH_SUCCESS_RATE                = 'FETCH_SUCCESS_RATE';
export const DELETE_RATE_SUCCESS               = 'DELETE_RATE_SUCCESS';
export const ERROR_DELETE_RATE                 = 'ERROR_DELETE_RATE';
export const SUCCESS_UPDATE_RATE               = 'SUCCESS_UPDATE_RATE';
export const ERROR_SUCCESS_UPDATE_RATE         = 'ERROR_SUCCESS_UPDATE_RATE';
export const RATE_UPDATE                       = 'RATE_UPDATE';
export const DELETE_RATE                       = 'DELETE_RATE';
export const REMOVE_ERROR_SUCCESS_UPDATE_RATE  = 'REMOVE_ERROR_SUCCESS_UPDATE_RATE';
export const REMOVE_DELETE_SUCCESS_RATE        = 'REMOVE_DELETE_SUCCESS_RATE';
export const REMOVE_SUCCESS_BANNER_UPDATE_RATE = 'REMOVE_SUCCESS_BANNER_UPDATE_RATE';
export const REMOVE_DELETE_SUCCESS_BANNER_RATE = 'REMOVE_DELETE_SUCCESS_BANNER_RATE';