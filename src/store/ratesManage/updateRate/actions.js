import {
    RATE_UPDATE,
    DELETE_RATE,  
    ERROR_DELETE_RATE,
    FETCH_RATE_INFO,
    FETCH_SUCCESS_RATE,
    ERROR_FETCH_RATE_INFO,
    SUCCESS_UPDATE_RATE,
    REMOVE_SUCCESS_BANNER_UPDATE_RATE,
    ERROR_SUCCESS_UPDATE_RATE,
    REMOVE_ERROR_SUCCESS_UPDATE_RATE,
    REMOVE_DELETE_SUCCESS_BANNER_RATE,
    
} from './actionTypes';

export const fetchRate = (rateId) => {
    return {
        type: FETCH_RATE_INFO,
        payload: rateId,
        fetchSuccess: false
    }
}

export const fetchRateSuccess = (rate) => {
        return {
            type: FETCH_SUCCESS_RATE,
            payload:rate,
            fetchSuccess: true
        }
}
export const fetchRateError = (error) => {
    return {
        type: ERROR_FETCH_RATE_INFO,
        payload: error
    }
}

export const updateRate = (rateId, rate, history) => {
    return {
        type: RATE_UPDATE,
        payload: {rateId, rate, history},
        fetchSuccess: false
    }
}

export const deleteRate = (rateId, history) => {
    return {
        type: DELETE_RATE,
        payload:{rateId, history},
        deleteSuccess: false
    }
}
export const deleteRateSuccess = (rate) => {
    return {
        type: DELETE_RATE,
        payload: rate,
        deleteSuccess: true
    }
}
export const deleteRateError = (error) => {
    return {
        type: ERROR_DELETE_RATE,
        deleteSuccess: false,
        error: error
    }
}

export const successUpdateRate = (rate) => {
    return {
        type: SUCCESS_UPDATE_RATE,
        payload: rate,
        updateSuccess: true
    }
}

export const removeSuccessBannerUpdateRate = () => {
    return {
        type: REMOVE_SUCCESS_BANNER_UPDATE_RATE
    }
}
export const successUpdateRateError = (error) => {
    return {
        type: ERROR_SUCCESS_UPDATE_RATE,
        payload: error,
        updateSuccess: false
    }
}
export const removeSuccessUpdateRateError = () => {
    return {
        type: REMOVE_ERROR_SUCCESS_UPDATE_RATE,
    }
}
export const removeSuccessDeleteBannerRate = () => {
    return {
        type: REMOVE_DELETE_SUCCESS_BANNER_RATE,
    }
}
