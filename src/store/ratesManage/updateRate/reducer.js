import {
    FETCH_RATE_INFO,       
    ERROR_FETCH_RATE_INFO, 
    DELETE_RATE_SUCCESS,
    FETCH_SUCCESS_RATE,
    ERROR_DELETE_RATE,
    SUCCESS_UPDATE_RATE,
    ERROR_SUCCESS_UPDATE_RATE,
    REMOVE_ERROR_SUCCESS_UPDATE_RATE,
    REMOVE_DELETE_SUCCESS_BANNER_RATE,
    REMOVE_SUCCESS_BANNER_UPDATE_RATE
} from './actionTypes';

const initialState = {
    error: "",
    updateSuccess: null,
    deleteSuccess: null,
    fetchSuccess: null,
    rateId: null,
    fetchRate: []
};

const updateRate = (state = initialState, action) => {

    switch(action.type) {
        case FETCH_RATE_INFO:
            return {
                ...state,
                fetchSuccess: false
            };
        case FETCH_SUCCESS_RATE:
            return {
                ...state,
                fetchSuccess: true,
                fetchRate: action.payload
            }
        case ERROR_FETCH_RATE_INFO:
            return {
                ...state,
                fetchSuccess: false,
                error: action.payload,
            }
        case DELETE_RATE_SUCCESS:
            return {
                ...state,
                deleteSuccess: true
            };
        case ERROR_DELETE_RATE:
            return {
                ...state,
                deleteSuccess: false,
                error: action.payload
            };
        case REMOVE_DELETE_SUCCESS_BANNER_RATE:
            return {
                ...state,
                deleteSuccess: null
            }
        case SUCCESS_UPDATE_RATE:
            return {
                ...state,
                updateSuccess: true
            };
        case ERROR_SUCCESS_UPDATE_RATE:
            return {
                ...state,
                updateSuccess: false,
                error: action.payload
            };
        case REMOVE_ERROR_SUCCESS_UPDATE_RATE:
            return {
                ...state,
                updateSuccess: null,
                error: null
            };
        case REMOVE_SUCCESS_BANNER_UPDATE_RATE:
            return {
                ...state,
                updateSuccess: null,
            };
        default:
            return {
                ...state
            }

    }
} 
export default updateRate;