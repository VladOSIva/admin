import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// Update/Delete user 
import {
    FETCH_RATE_INFO,       
    RATE_UPDATE,
    DELETE_RATE
} from './actionTypes';

import axios from 'axios';

import {
    fetchRateError,
    fetchRateSuccess,
    deleteRateSuccess,
    deleteRateError,
    removeSuccessDeleteBannerRate,
    successUpdateRate,
    successUpdateRateError,
    removeSuccessUpdateRateError,
    removeSuccessBannerUpdateRate
} from './actions';

const delay = time => new Promise(resolve => setTimeout(resolve, time));

async function fetchRateInfo(urlId) {
    
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };

    try {

        const response = await axios.get(urlId, config);
        return await response.data

    } catch(e) {

        throw e.response.data

    }
};
async function rateUpdateInfo(urlId,{name,email,password, confirm_password, permission}) {

    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };

    const body = JSON.stringify({name,email,password, confirm_password,permission});
    
    try {
        
        const response = await axios.put(urlId, body, config);
        return await response.data
    
    } catch(e) {
    
        throw e.response.data
    
    }
};

async function rateDeleteReq(urlId) {
    
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };
    
    try {
        
        const response = await axios.delete(urlId, config);
        return await response.data
    
    } catch(e) {
    
        throw e.response.data
    
    }
};

function* rateDelete({payload: {rateId, history}}) {
    try { 
        console.log(rateId);
        const response = yield (call(rateDeleteReq, `${process.env.REACT_APP_BACKEND}/api/rates/${rateId}`));
        yield put(deleteRateSuccess(response));
        history.push('/administration/rates/');
        yield call(delay, 2000);
        yield put(removeSuccessDeleteBannerRate());

    } catch(e) {

        yield put(deleteRateError(e));

    }
};

function* updateRate ({payload: {rate, history, rateId}}) {

    try { 
        const response = yield (call(rateUpdateInfo, `${process.env.REACT_APP_BACKEND}/api/rates/${rateId}`,
            {   
                name: rate.name,
                rate: rate.rate,
                min_sum: rate.min_sum,
                max_sum: rate.max_sum,
                term_deposit: rate.term_deposit,
                status: rate.status
            }));

            yield put(successUpdateRate(response));
            history.push('/administration/rates/');
            yield call(delay, 2000);
            yield put(removeSuccessBannerUpdateRate());
        
    } catch(e) {
        yield put(successUpdateRateError(e));
        yield call(delay, 2000);
        yield put(removeSuccessUpdateRateError());
    }
};

function* fetchRateUpdate({payload}) {

    try { 
        const response = yield (call(fetchRateInfo, `${process.env.REACT_APP_BACKEND}/api/rates/${payload}`))
        yield put(fetchRateSuccess(response))

    } catch(e) {
        yield put(fetchRateError(e))
    }
};

export function* watchFetchRateUpdate() {
    yield takeEvery(FETCH_RATE_INFO, fetchRateUpdate)
};

export function* watchRateUpdate() {
    yield takeEvery(RATE_UPDATE, updateRate)
};

export function* watchRateDelete() {
    yield takeEvery(DELETE_RATE, rateDelete)
};

function* UpdateRateSaga() {
    yield all([
        fork(watchFetchRateUpdate),
        fork(watchRateUpdate),
        fork(watchRateDelete)
    ])
};

export default UpdateRateSaga