import { combineReducers } from "redux";

// Front
import Layout from "./layout/reducer";

// Authentication
import Login from "./auth/login/reducer";
import Account from "./auth/register/reducer";
import ForgetPassword from "./auth/forgetpwd/reducer";
import Profile from "./auth/profile/reducer";

// UserAdmin
import FetchUsersList from './userManage/listUsers/reducer';
// UserCreate
import CreateNewUser from './userManage/createUser/reducer';
import UpdateUser from './userManage/updateUser/reducer';
// Client
import FetchClientsList from './clientsManage/listClients/reducer';
import CreateNewClient from './clientsManage/createClient/reducer';
import UpdateClient from './clientsManage/updateClient/reducer';
// Plan
import FetchPlansList from './plansManage/listPlans/reducer';
import CreateNewPlan from './plansManage/createPlans/reducer';
import UpdatePlan from './plansManage/updatePlan/reducer';
// Rate
import FetchRatesList from './ratesManage/listRates/reducer';
import UpdateRate from './ratesManage/updateRate/reducer';

const rootReducer = combineReducers({
  // public
  Layout,
  Login,
  Account,
  ForgetPassword,
  Profile,
  FetchUsersList,
  CreateNewUser,
  UpdateUser,
  FetchClientsList,
  CreateNewClient,
  UpdateClient,
  FetchPlansList,
  CreateNewPlan,
  UpdatePlan,
  FetchRatesList,
  UpdateRate,
});

export default rootReducer;
