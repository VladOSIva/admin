import { all } from 'redux-saga/effects';

//public
import AccountSaga from './auth/register/saga';
import AuthSaga from './auth/login/saga';
import ForgetSaga from './auth/forgetpwd/saga';
import ProfileSaga from './auth/profile/saga';
import LayoutSaga from './layout/saga';

// UserAdmin
import UsersFetchSaga from './userManage/listUsers/saga';
// CreateNewUser 
import CreateUserSaga from './userManage/createUser/saga';
// UpdateUser
import UpdateUserSaga from './userManage/updateUser/saga';
// ListClients
import ClientsFetchSaga from './clientsManage/listClients/saga';
// CreateClient
import CreateClientSaga from './clientsManage/createClient/saga';
import UpdateClientSaga from './clientsManage/updateClient/saga';
// ListPlan
import PlansFetchSaga from './plansManage/listPlans/saga';
// CreatePlan
import CreatePlanSaga from './plansManage/createPlans/saga';
// UpdatePlan
import UpdatePlanSaga from './plansManage/updatePlan/saga';
// ListRate
import RatesFetchSaga from './ratesManage/listRates/saga';
// UpdateRate


// Warning you need to supervise your import connection you have fucked up with importing the chunk under. 
// You have had /ratesManage/listRates/saga instead the example under
import UpdateRateSaga from './ratesManage/updateRate/saga';

export default function* rootSaga() {
    yield all([
        //public
        AccountSaga(),
        AuthSaga(),
        ProfileSaga(),
        ForgetSaga(),
        LayoutSaga(),
        UsersFetchSaga(),
        CreateUserSaga(),
        UpdateUserSaga(),
        ClientsFetchSaga(),
        CreateClientSaga(),
        UpdateClientSaga(),
        PlansFetchSaga(),
        CreatePlanSaga(),
        UpdatePlanSaga(),
        RatesFetchSaga(),
        UpdateRateSaga()
    ])
}