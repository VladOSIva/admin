// Action types for create user
export const CREATE_USER             = 'CREATE_USER';
export const SUCCESS_CREATE_USER     = 'SUCCESS_CREATE_USER';
export const FAIL_CREATE_USER        = 'FAIL_CREATE_USER';
export const REMOVE_FAIL_CREATE_USER = 'REMOVE_FAIL_CREATE_USER';
export const REMOVE_SUCCESS_CREATE_BANNER = 'REMOVE_SUCCESS_CREATE_BANNER';