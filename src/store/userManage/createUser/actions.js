import {
    SUCCESS_CREATE_USER, 
    FAIL_CREATE_USER,
    REMOVE_FAIL_CREATE_USER,
    CREATE_USER,
    REMOVE_SUCCESS_CREATE_BANNER
} from './actionTypes';

export const createUser = (user, history) => {
    return {
        type: CREATE_USER,
        payload:{user, history},
        success: false
    }
}
export const successCreateUser = (user) => {
    return {
        type: SUCCESS_CREATE_USER,
        payload: user,
        success: true
    }
}
export const removeCreateSuccessBanner = () => {
    return {
        type: REMOVE_SUCCESS_CREATE_BANNER,
        success: false
    }
}
export const apiError = (error) => {
    return {
        type: FAIL_CREATE_USER,
        payload: error
    }
}
export const removeApiError = () => {
    return {
        type: REMOVE_FAIL_CREATE_USER
    }
}