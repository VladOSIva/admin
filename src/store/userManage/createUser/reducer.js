import {
    SUCCESS_CREATE_USER, 
    FAIL_CREATE_USER,
    REMOVE_FAIL_CREATE_USER,
    REMOVE_SUCCESS_CREATE_BANNER
} from './actionTypes';

const initialState = {
    error:"",
    success: null,
    user: []
}


const fetchUsersList = (state = initialState, action) => {

    switch(action.type) {
        case SUCCESS_CREATE_USER:
            return {
                ...state,
                success: true,
                user: action.payload
            };
        case FAIL_CREATE_USER:
            return {
                ...state,
                success: false,
                error: action.payload,
            };
        case REMOVE_FAIL_CREATE_USER:
            return {
                ...state,
                success: false, 
                error: ""
            };
        case REMOVE_SUCCESS_CREATE_BANNER:
            return {
                ...state,
                success: null
            };
        default:
            return {
                ...state
            }
    }
};
export default fetchUsersList;