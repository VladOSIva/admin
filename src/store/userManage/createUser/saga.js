import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// FetchUser Redux States 
import { CREATE_USER } from './actionTypes';
import { successCreateUser, apiError, removeApiError, removeCreateSuccessBanner} from './actions';
import axios from 'axios';
// delay action with calling
const delay = time => new Promise(resolve => setTimeout(resolve, time));

async function createNewUser(url,{name,email,password,confirm_password}) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
     }
    const body = JSON.stringify({name,email,password, confirm_password});
    try { 
    const response = await axios.post(url, body, config);
    return  await response.data
    } catch(e) {
        throw e.response.data
    }
};
function* createUser({ payload: { user, history } }) {
    try {
        const response = yield (call(createNewUser, `${process.env.REACT_APP_BACKEND}/api/users`, {name: user.name, email: user.email, password: user.password, confirm_password: user.confirm_password})) 
        yield put(successCreateUser(response))
        history.push('/administration/users/')
        yield call(delay, 4000)
        yield put(removeCreateSuccessBanner())

    } catch(e) {
        yield put(apiError(e.errors));
        yield call(delay, 1000)
        yield put(removeApiError());
      
    }
}

export function* watchCreateUser() {
    yield takeEvery(CREATE_USER, createUser)
}
function* CreateUserSaga() {
    yield all([
        fork(watchCreateUser)
    ])
}

export default CreateUserSaga