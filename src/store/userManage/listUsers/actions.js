import {
    FETCH_ERROR,
    FETCH_SUCCESS,
    FETCH_USERS,
} from './actionTypes';

export const fetchUsers = (users, history) => {
    return { 
        type: FETCH_USERS,
        payload: {users, history}
    }
};
export const fetchSuccess = (users) => {
    return {
        type: FETCH_SUCCESS,
        payload: users
    }
}
export const errorFetch = (error) => {
    return {
        type: FETCH_ERROR,
        payload: error
    }
}
