import {
    FETCH_ERROR,
    FETCH_SUCCESS,
    FETCH_USERS,
} from './actionTypes';

const initialState = {
    error: "",
    loading: null, 
    users: [],
};

const fetchUsersList = (state = initialState, action) => {

    switch(action.type) {
        case FETCH_USERS:
            return {
                ...state, 
                loading: true, 
            };
        case FETCH_SUCCESS:
            return {
                ...state,
                loading: false,
                users: action.payload,
            };
        case FETCH_ERROR:
            return {
                ...state,
                error: action.payload, loading:true
            };
        default:
            return {
                ...state
            }
    }
};
export default fetchUsersList