import { takeEvery, fork, put, all, call, select} from 'redux-saga/effects';
// FetchUser Redux States 
import { FETCH_USERS } from './actionTypes';
import { fetchSuccess, errorFetch, } from './actions';
import axios from 'axios';

async function fetchUsersList (url) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    };
    try {
        const response = await axios.get(url,config)
        return await response.data
    } catch(e) {
        throw e
    }
};

function* fetchList() {
    try {
        const response = yield call(fetchUsersList, `${process.env.REACT_APP_BACKEND}/api/users/`);
        yield put(fetchSuccess(response))
        
    } catch (e) {
        yield put(errorFetch('Ошибка повторите снова'));
    }
};
export function* watchUsersFetching() {
    yield takeEvery(FETCH_USERS, fetchList)
};

function* UsersFetchSaga() {
    yield all([
        fork(watchUsersFetching)
    ]) 
};

export default UsersFetchSaga;