import {
    FETCH_USER_INFO,
    FETCH_SUCCESS_USER,       
    ERROR_FETCH_USER_INFO, 
    DELETE_USER_SUCCESS,   
    ERROR_DELETE_USER,
    USER_UPDATE,
    SUCCESS_UPDATE,
    DELETE_USER,
    ERROR_SUCCESS_UPDATE,
    REMOVE_ERROR_SUCCESS_UPDATE,
    REMOVE_SUCCESS_BANNER_UPDATE,
    REMOVE_DELETE_SUCCESS_BANNER
} from './actionTypes';

export const fetchUser = (userId) => {
    return {
        type: FETCH_USER_INFO,
        payload: userId,
        fetchSuccess: false
    }
}
export const fetchUserSuccess = (user) => {
        return {
            type: FETCH_SUCCESS_USER,
            payload:user,
            fetchSuccess: true
        }
}
export const fetchUserError = (error) => {
    return {
        type: ERROR_FETCH_USER_INFO,
        payload: error
    }
}
export const deleteUser = (userId, history) => {
    return {
        type: DELETE_USER,
        payload:{userId, history},
        deleteSuccess: false
    }
}
export const deleteUserSuccess = (user) => {
    return {
        type: DELETE_USER_SUCCESS,
        payload: user,
        deleteSuccess: true
    }
}
export const deleteUserError = (error) => {
    return {
        type: ERROR_DELETE_USER,
        deleteSuccess: false,
        error: error
    }
}

export const updateUser = (userId,user,history) => {
    return {
        type: USER_UPDATE,
        payload: {userId,user,history},
        fetchSuccess: false
    }
}
export const successUpdateUser = (user) => {
    return {
        type: SUCCESS_UPDATE,
        payload: user,
        updateSuccess: true
    }
}
export const removeSuccessBannerUpdate = () => {
    return {
        type: REMOVE_SUCCESS_BANNER_UPDATE
    }
}
export const successUpdateUserError = (error) => {
    return {
        type: ERROR_SUCCESS_UPDATE,
        payload: error,
        updateSuccess: false
    }
}
export const removeSuccessUpdateUserError = () => {
    return {
        type: REMOVE_ERROR_SUCCESS_UPDATE,
    }
}
export const removeSuccessDeleteBanner = () => {
    return {
        type: REMOVE_DELETE_SUCCESS_BANNER,
    }
}