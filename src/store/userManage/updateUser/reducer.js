import {
    FETCH_USER_INFO,       
    ERROR_FETCH_USER_INFO, 
    DELETE_USER_SUCCESS, 
    FETCH_SUCCESS_USER,  
    ERROR_DELETE_USER,
    SUCCESS_UPDATE,
    ERROR_SUCCESS_UPDATE,
    REMOVE_ERROR_SUCCESS_UPDATE,
    REMOVE_SUCCESS_BANNER_UPDATE,
    REMOVE_DELETE_SUCCESS_BANNER
} from './actionTypes';

const initialState = {
    error: "",
    updateSuccess: null,
    deleteSuccess: null,
    fetchSuccess: null,
    userId: null,
    fetchUser: []
};

const updateUser = (state = initialState, action) => {

    switch(action.type) {
        case FETCH_USER_INFO:
            return {
                ...state,
                fetchSuccess: false
            };
        case FETCH_SUCCESS_USER:
            return {
                ...state,
                fetchSuccess: true,
                fetchUser: action.payload
            }
        case ERROR_FETCH_USER_INFO:
            return {
                ...state,
                fetchSuccess: false,
                error: action.payload,
            }
        case DELETE_USER_SUCCESS:
            return {
                ...state,
                deleteSuccess: true
            };
        case ERROR_DELETE_USER:
            return {
                ...state,
                deleteSuccess: false,
                error: action.payload
            };
        case REMOVE_DELETE_SUCCESS_BANNER:
            return {
                ...state,
                deleteSuccess: null
            }
        case SUCCESS_UPDATE:
            return {
                ...state,
                updateSuccess: true
            };
        case ERROR_SUCCESS_UPDATE:
            return {
                ...state,
                updateSuccess: false,
                error: action.payload
            };
        case REMOVE_ERROR_SUCCESS_UPDATE:
            return {
                ...state,
                updateSuccess: null,
                error: null
            };
        case REMOVE_SUCCESS_BANNER_UPDATE:
            return {
                ...state,
                updateSuccess: null,
            };
        default:
            return {
                ...state
            }

    }
}
export default updateUser;