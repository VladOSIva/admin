import { takeEvery, fork, put, all, call} from 'redux-saga/effects';
// Update/Delete user 
import {
    FETCH_USER_INFO,       
    USER_UPDATE,
    DELETE_USER,
} from './actionTypes';

import axios from 'axios';

import {
     fetchUserError, 
    fetchUserSuccess,
    deleteUserSuccess, deleteUserError,
    successUpdateUser, successUpdateUserError, removeSuccessUpdateUserError, removeSuccessDeleteBanner,
    removeSuccessBannerUpdate
} from './actions';

const delay = time => new Promise(resolve => setTimeout(resolve, time));

async function fetchUserInfo(urlId) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    }
    try {
        const response = await axios.get(urlId, config)
        return await response.data
    } catch(e) {
        throw e.response.data
    }
}
async function userUpdateInfo(urlId,{name,email,password, confirm_password, permission}) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    }
    const body = JSON.stringify({name,email,password, confirm_password,permission});
    try {
        
        const response = await axios.put(urlId, body, config)
        return await response.data
    } catch(e) {
        throw e.response.data
    }
}
async function userDeleteReq(urlId) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${JSON.parse(localStorage.authUser).access_token}`
        }
    }
    try {
        
        const response = await axios.delete(urlId, config)
        return await response.data
    } catch(e) {
        throw e.response.data
    }
}

function* userDelete({payload}) {

    try { 
        const response = yield (call(userDeleteReq, `${process.env.REACT_APP_BACKEND}/api/users/${payload.userId}`))
        yield put(deleteUserSuccess(response))
        payload.history.push('/administration/users/')
        yield call(delay, 2000)
        yield put(removeSuccessDeleteBanner())
    } catch(e) {
        yield put(deleteUserError(e))
    }
}

function* updateUser({payload}) {

    try { 
        const response = yield (call(userUpdateInfo, `${process.env.REACT_APP_BACKEND}/api/users/${payload.userId}`,
         {name:payload.user.name, email:payload.user.email, password:payload.user.password, confirm_password:payload.user.confirm_password, permission:payload.user.permission}))
            yield put(successUpdateUser(response))
            payload.history.push('/administration/users/')
            yield call(delay, 2000)
            yield put(removeSuccessBannerUpdate())
        
    } catch(e) {
        yield put(successUpdateUserError(e))
        yield call(delay, 2000)
        yield put(removeSuccessUpdateUserError())
    }
}

function* fetchUserUpdate({payload}) {

    try { 
        const response = yield (call(fetchUserInfo, `${process.env.REACT_APP_BACKEND}/api/users/${payload}`))
        yield put(fetchUserSuccess(response))

    } catch(e) {
        yield put(fetchUserError(e))
    }
}

export function* watchFetchUserUpdate() {
    yield takeEvery(FETCH_USER_INFO, fetchUserUpdate)
}
export function* watchUserUpdate() {
    yield takeEvery(USER_UPDATE, updateUser)
}
export function* watchUserDelete() {
    yield takeEvery(DELETE_USER, userDelete)
}
function* UpdateUserSaga() {
    yield all([
        fork(watchFetchUserUpdate),
        fork(watchUserUpdate),
        fork(watchUserDelete)
    ])
}
export default UpdateUserSaga