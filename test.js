const axios = require('axios');
async function loginData (url,{email, password}) {
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify({email,password});
    try { 
    const response = await axios.post(url, body, config);
    console.log(response.data)
    return  await response.data
    } catch(e) {
        console.log(e.response.data)
        return e.response.data
    }
}
loginData('https://api-birja.landing-test.net.ua/api/auth/login',{email:"admin@mail.com", password:"admind"})